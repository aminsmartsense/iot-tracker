import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {UserAuthModule} from "./user-auth/user-auth.module";
import {AppRoutingModule} from "./app.routing.module";
import {globalToastConfig} from '@constants/base-constants';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AdminAuthGuard} from "./_guards/auth.gaurds";
import {UtilityModule} from "./utility/utility.module";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(globalToastConfig), // ToastrModule added
    UserAuthModule,
    UtilityModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [
    AdminAuthGuard
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule {
}
