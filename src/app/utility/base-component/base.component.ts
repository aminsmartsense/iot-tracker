import {Component, OnInit} from '@angular/core';
import {PopupState} from "../constants/base-constants";
import {ClipboardText, SetupScrollClass} from "../shared-functions/common-functions";

declare var $;

@Component({
  selector: 'app-base-component',
  templateUrl: 'base.component.html'
})
export class BaseComponent implements OnInit {

  isCopied = false;
  type: any = 'basic';

  constructor() {
  }

  ngOnInit() {
  }

  // Form Validation Methods
  isRequiredField(formControlName) {
    return formControlName.hasError('required') && formControlName.touched;
  }

  isValidField(formControlName) {
    return formControlName.hasError('pattern');
  }

  isValidLength(formControlName) {
    return formControlName.hasError('minlength') || formControlName.hasError('maxlength');
  }

  hasError(errorName, formGroup, formControl) {
    return formGroup.hasError(errorName) && formControl.touched;
  }

  isRequiredFieldCheck(currentControl, controlToCheck) {
    return (!currentControl.value) && controlToCheck.touched;
  }

  // scroll function
  onScroll() {
    $('html, body').animate({scrollTop: 0}, 'slow');
  }

  // get html code
  getHtmlCode(className: any) {
    return this.querySelector(className);
  }

  querySelector(className) {
    return document.querySelectorAll('.' + className)[0]['outerHTML'];
  }

  onCopySingle(val) {
    this.isCopied = ClipboardText(val);
    setTimeout(() => {
      this.isCopied = false;
    }, 1000);
  }

  onCopyFile(file: string) {
    this.onCopySingle(file);
  }

  // Modal vertically center
  onOpenModal(id) {
    SetupScrollClass(PopupState.Open);
    setTimeout(() => {
      document.getElementById(id).style.top = ((window.innerHeight) / 2) - ((document.getElementById(id).offsetHeight) / 2) + 'px' ;
      document.getElementById(id).style.opacity = '1';
    }, 5);
  }

  // Hide body scroll
  onRemoveScroll() {
    SetupScrollClass(PopupState.Close);
  }
}
