import {PopupState} from "../constants/base-constants";
declare var $;

export const isEmpty = (obj): boolean => Object.keys(obj).length === 0 && obj.constructor === Object;

export const AppLogger = (value: any) => {
  // console.log(`<------------------------------------ ${value} ------------------------------------>`);
};


export function ClipboardText(value: string): boolean {
  try {
    const inp = document.createElement('input');
    document.body.appendChild(inp);
    inp.value = value;
    inp.select();
    document.execCommand('copy', false);
    inp.remove();
    return true;
  } catch (e) {
    return false;
  }
}

export function SetupScrollClass(popupState: PopupState) {
  popupState === PopupState.Open ? $("body").addClass("scroll-hide") : $("body").removeClass("scroll-hide");
}

