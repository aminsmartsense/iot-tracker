import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatInputModule,
  MatPaginatorModule
} from "@angular/material";

@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatPaginatorModule
  ],
  exports: [
    MatInputModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatPaginatorModule
  ]
})
export class MaterialModule {
}
