import {Directive, Input, ElementRef, Renderer} from '@angular/core';

@Directive({
  selector: '[focus]'
})
export class FocusDirective {
  @Input() focus: boolean;
  private el: any;

  constructor(private elementRef: ElementRef,) {
    this.el = this.elementRef.nativeElement;
  }

  ngOnInit() {
    this.el.focus();
  }

}
