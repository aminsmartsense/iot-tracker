import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
})
export class ToastComponent implements OnInit {
  @Input() messages: any;
  
  constructor() {
  }
  
  ngOnInit() {
  }
  
}
