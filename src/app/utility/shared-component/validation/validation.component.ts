import {Component, Input} from '@angular/core';

@Component({
  selector: 'validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.css']
})

export class ValidationComponent {
  @Input() errMsg: string;
}
