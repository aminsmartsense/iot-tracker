import * as CryptoJS from "crypto-js";
import {BASE} from "@constants/base-constants";

export class CommonFunctions {


  public static ENCRYPT_OBJ(value: any): any {
    return CryptoJS.AES.encrypt(JSON.stringify(value), BASE.ENCRYPTION_TOKEN);
  }

  public static DECRYPT_OBJ(value: any): any {
    if (value && value != null) {
      const bytes = CryptoJS.AES.decrypt(value.toString(), BASE.ENCRYPTION_TOKEN);
      const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      return decryptedData;
    }
    return '';
  }
}




