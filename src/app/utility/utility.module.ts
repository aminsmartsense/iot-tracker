import {ModuleWithProviders, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BaseComponent} from "./base-component/base.component";
import {PageNotFoundComponent} from "./shared-component/page-not-found/page-not-found.component";
import {ProgressHudComponent} from "./shared-component/progress-hud/progress-hud.component";
import {FocusDirective} from "./directives/focus.directive";
import {DisableControlDirective} from "./directives/disable-control.directive";
import {CheckEmptyPipe, PlaceNAPipe} from "./pipes/checkEmpty.pipe";
import {SortPipe} from "./pipes/filter-pipe.pipe";
import {LocaleNumberPipe} from "./pipes/locale-number.pipe";
import {FormatTimePipe} from "./pipes/timer.pipe";
import {HttpInterceptors} from "./http-interceptors/index-Interceptor";
import {ToastComponent} from "./shared-component/toast/toast.component";
import {SharedService} from "@services/shared.service";
import {APIManager} from "@services/apimanager.service";
import {SharedUserService} from "@services/shared-user.service";
import {MaterialModule} from "./shared-module/material/material.module";
import {ValidationComponent} from "./shared-component/validation/validation.component";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    MaterialModule
  ],
  declarations: [
    BaseComponent,
    PageNotFoundComponent,
    ProgressHudComponent,
    ValidationComponent,
    ToastComponent,
    FocusDirective,
    DisableControlDirective,
    CheckEmptyPipe,
    PlaceNAPipe,
    SortPipe,
    LocaleNumberPipe,
    FormatTimePipe,
  ],
  providers: [],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    CommonModule,
    FocusDirective,
    ReactiveFormsModule,
    BaseComponent,
    ValidationComponent,
    DisableControlDirective
  ]
})
export class UtilityModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: UtilityModule,
      providers: [HttpInterceptors,
        SharedService,
        SharedUserService,
        APIManager]
    };
  }
}
