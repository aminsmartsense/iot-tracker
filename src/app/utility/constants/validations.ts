export class CommonRegexp {
  public static NUMERIC_REGEXP = '^[0-9]*$';
  public static ONLY_ALPHA_REGEXP = '^[a-zA-Z]*$';
  public static ALPHA_NUMERIC_REGEXP = '^[A-Za-z0-9]*$';
  public static USER_NAME_REGEXP = '^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z\\d#?.!@$%^&*-]+$';
  public static EMAIL_ADDRESS_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public static PASSWORD_REGEXP = /^(?=.*[a-zA-Z])(?=.*[A-Za-z])(?=.*\d)[a-zA-Z\d#?!@$%^&*-]{8,}$/;
  public static MAC_ADDRESS_REGEXP = /^(([A-Fa-f0-9]{2}[:]){7}[A-Fa-f0-9]{2}[,]?)+$/;
  public static ALPHANUMERIC_SPACE_REGEXP = '^[a-zA-Z0-9 ]*$';
}

export class ValidationConstant {
  public IS_REQUIRED = ' is required';
  public USERNAME = 'UserName' + this.IS_REQUIRED.replace('is', 'are');
  public VALID_EMAIL = 'Enter valid email address';
  public VALID_DOCUMENT_TYPE = 'Valid Document type is PDF';
  public VALID_IMAGE_TYPE = 'Valid Image types are png, jpg, jpeg';

  /*Email*/
  public static EMAIL_MIN_LENGTH = 6;
  public static EMAIL_MAX_LENGTH = 40;
  public EMAIL_REQUIRED = 'Email address' + this.IS_REQUIRED;
  public EMAIL_VALID = 'Enter valid email address';
  public EMAIL_LENGTH = 'email address length between' + ValidationConstant.EMAIL_MIN_LENGTH + ' to ' + ValidationConstant.EMAIL_MAX_LENGTH;


  public static USERNAME_MIN_LENGTH = 2;
  public static USERNAME_MAX_LENGTH = 30;

  public static ZONE_MIN_LENGTH = 2;
  public static ZONE_MAX_LENGTH = 30;

  public static DEVICE_MIN_LENGTH = 2;
  public static DEVICE_MAX_LENGTH = 30;

  public static FIRST_NAME_MIN_LENGTH = 2;
  public static FIRST_NAME_MAX_LENGTH = 30;

  public static LAST_NAME_MIN_LENGTH = 2;
  public static LAST_NAME_MAX_LENGTH = 30;

  /*Password*/
  public static PASSWORD_MIN_LENGTH = 8;
  public PASSWORD_REQUIRED = 'Password' + this.IS_REQUIRED;
  public PASSWORD_VALID = 'Enter valid password';
  public PASSWORD_LENGTH = 'Password minimum length is ' + ValidationConstant.PASSWORD_MIN_LENGTH;

  public USERNAME_REQUIRED = 'Username' + this.IS_REQUIRED;
  public USERNAME_LENGTH = 'Username length between ' + ValidationConstant.USERNAME_MIN_LENGTH + ' to ' + ValidationConstant.USERNAME_MAX_LENGTH;
  public USERNAME_NOT_VALID = 'Username is not valid';

  public ZONE_NAME_LENGTH = 'Zone name length between ' + ValidationConstant.ZONE_MIN_LENGTH + ' to ' + ValidationConstant.ZONE_MAX_LENGTH;
  public ZONE_NAME_VALID = 'Enter valid zone name';

  public DEVICE_NAME_LENGTH = 'Device name length between ' + ValidationConstant.DEVICE_MIN_LENGTH + ' to ' + ValidationConstant.DEVICE_MAX_LENGTH;
  public DEVICE_NAME_VALID = 'Enter valid device name';

  public NAME_REQUIRED = 'Name' + this.IS_REQUIRED;

  public FIRST_NAME_REQUIRED = 'First name' + this.IS_REQUIRED;
  public FIRST_NAME_LENGTH = 'First name length between ' + ValidationConstant.FIRST_NAME_MIN_LENGTH + ' to ' + ValidationConstant.FIRST_NAME_MAX_LENGTH;
  public FIRST_NAME_VALID = 'Enter valid first name';

  public LAST_NAME_REQUIRED = 'Last name' + this.IS_REQUIRED;
  public LAST_NAME_LENGTH = 'Last name length between ' + ValidationConstant.LAST_NAME_MIN_LENGTH + ' to ' + ValidationConstant.LAST_NAME_MAX_LENGTH;
  public LAST_NAME_VALID = 'Enter valid last name';

  public MAC_ADDRESS_REQUIRED = 'Mac address' + this.IS_REQUIRED;
  public MAC_ADDRESS_VALID = 'Enter valid Mac address';

  public DATE_REQUIRED = 'Date' + this.IS_REQUIRED;
}
