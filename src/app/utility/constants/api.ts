import {BASE} from './base-constants';

export class API {
  public static LOGIN = BASE.API_URL + 'login';
  public static ADD_DEVICE = BASE.API_URL + 'api/device';
  public static GET_DEVICE = BASE.API_URL + 'api/device/all';
  public static GET_ZONE = BASE.API_URL + 'api/zone/all';
  public static ADD_ZONE = BASE.API_URL + 'api/zone';
  public static GET_USERS = BASE.API_URL + 'api/user/all';
  public static ADD_USERS = BASE.API_URL + 'api/user';
  public static GET_ZONE_NEIGHBOUR = BASE.API_URL + 'api/gatewayneighbour';
  public static GET_DEVICE_NEIGHBOUR = BASE.API_URL + 'api/deviceneighbour';
}
