export class RouteConstants {
  public static ADMIN = 'admin';
  public static LOGIN = 'login';

  /*For library*/
  public static DASHBOARD_ROUTE = 'dashboard';

  public static ZONES_ROUTE = 'zones';
  public static ZONES_DETAILS_ROUTE = 'zone/details';

  public static USERS_ROUTE = 'users';
  public static USERS_ADD_ROUTE = 'users/add';

  public static DEVICES_ROUTE = 'devices';
  public static DEVICES_DETAILS_ROUTE = 'device/details';

  public static DASHBOARD = '/' + RouteConstants.ADMIN + '/' + RouteConstants.DASHBOARD_ROUTE;
  public static ZONES = '/' + RouteConstants.ADMIN + '/' + RouteConstants.ZONES_ROUTE;
  public static ZONE_DETAILS = '/' + RouteConstants.ADMIN + '/' + RouteConstants.ZONES_DETAILS_ROUTE;
  public static USERS = '/' + RouteConstants.ADMIN + '/' + RouteConstants.USERS_ROUTE;
  public static USERS_ADD = '/' + RouteConstants.ADMIN + '/' + RouteConstants.USERS_ADD_ROUTE;
  public static DEVICES = '/' + RouteConstants.ADMIN + '/' + RouteConstants.DEVICES_ROUTE;
  public static DEVICE_DETAILS = '/' + RouteConstants.ADMIN + '/' + RouteConstants.DEVICES_DETAILS_ROUTE;
}
