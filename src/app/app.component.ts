import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {individualToastConfig, ToastStatus} from '@constants/base-constants';
import {SharedService} from "@services/shared.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  toastSubscriber$: any;

  constructor(private toastr: ToastrService,
              private _sharedService: SharedService) {
  }

  ngOnInit() {
    this.configureToastManager();
  }

  configureToastManager() {
    const individualConfig = {};
    this.toastSubscriber$ = this._sharedService.getToastMessage().subscribe(msgBody => {
      if (msgBody) {
        if (msgBody.type === ToastStatus.SUCCESS) {
          setTimeout(() => this.toastr.success(msgBody.message, msgBody.title, individualToastConfig));
        } else if (msgBody.type === ToastStatus.ERROR) {
          setTimeout(() => this.toastr.error(msgBody.message, msgBody.title, individualToastConfig));
        }
        this._sharedService.setToastMessage('', ToastStatus.UNKNOWN);
      }
    });
  }

  /*subscribeLoggedin() {
    this.loginSubscriber = this.sharedService.getLoginRequired().subscribe(value => {
      this.isLoggedIn = value;
    });
  }

  ngOnDestroy() {
    if (this.loginSubscriber) {
      this.loginSubscriber.unsubscribe();
    }
  }*/
}
