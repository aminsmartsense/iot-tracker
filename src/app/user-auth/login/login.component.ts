import {Component, OnInit} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CommonRegexp, ValidationConstant} from "../../utility/constants/validations";
import {BaseComponent} from "../../utility/base-component/base.component";
import {Router} from "@angular/router";
import {RouteConstants} from "../../utility/constants/routes";
import {SharedService} from "../../utility/shared-service/shared.service";
import {LoginService} from "./login.service";
import {ToastStatus} from "@constants/base-constants";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})
export class LoginComponent extends BaseComponent implements OnInit {

  // Form variables
  loginForm: FormGroup;

  // validations message
  validationMsg = new ValidationConstant();

  constructor(private router: Router, private _sharedService: SharedService, private _loginService: LoginService) {
    super();
  }

  ngOnInit() {
    this.initializeMethods();
    //this._sharedService.setToastMessage('hey toast is working', ToastStatus.SUCCESS);
  }

  /**
   * Initialize method
   */
  initializeMethods() {
    this.createLoginForm();
  }

  /**
   * Create login form
   */
  createLoginForm() {
    this.loginForm = new FormGroup({
      userName: new FormControl('', [
        Validators.required, Validators.pattern(CommonRegexp.ALPHA_NUMERIC_REGEXP),
        Validators.minLength(ValidationConstant.USERNAME_MIN_LENGTH),
        Validators.maxLength(ValidationConstant.USERNAME_MAX_LENGTH)
      ]),
      password: new FormControl('', [
        Validators.required, Validators.pattern(CommonRegexp.PASSWORD_REGEXP),
        Validators.minLength(ValidationConstant.PASSWORD_MIN_LENGTH)
      ])
    });
  }

  /**
   * On Login form submit
   * @param value
   * @param valid
   */
  onLoginFormSubmit(value, valid) {
    if (valid) {
      this._loginService.login(value).subscribe(response => {
        this._sharedService.setLoginRequired(true);
        this.router.navigate(['/' + RouteConstants.DASHBOARD]);
      });
    }
  }

}
