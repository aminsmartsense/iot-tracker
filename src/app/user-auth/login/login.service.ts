import { Injectable } from '@angular/core';
import {APIManager} from "@services/apimanager.service";
import {Observable} from "rxjs/index";
import {API} from "@constants/api";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private _apiManager: APIManager) { }

  login(params): Observable<any> {
    return this._apiManager.postAPI(API.LOGIN, params, this._apiManager.HttpOptions_2, true, false);
  }
}
