import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserAuthRoutingModule} from './user-auth-routing.module';
import {LoginComponent} from './login/login.component';
import {UtilityModule} from "../utility/utility.module";

@NgModule({
  imports: [
    CommonModule,
    UtilityModule,
    UserAuthRoutingModule
  ],
  declarations: [
    LoginComponent
  ]
})
export class UserAuthModule {
}
