import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RouteConstants} from "../utility/constants/routes";
import {LoginComponent} from "./login/login.component";
import {AdminAuthGuard} from "../_guards/auth.gaurds";

const routes: Routes = [
  {
    path: RouteConstants.LOGIN,
    component: LoginComponent,
    canActivate: [AdminAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserAuthRoutingModule {
}
