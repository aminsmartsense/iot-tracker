import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RouteConstants} from "./utility/constants/routes";

const routes: Routes = [
  {
    path: RouteConstants.ADMIN,
    loadChildren: './admin/admin.module#AdminModule'
  },
  {
    path: '**',
    redirectTo: RouteConstants.LOGIN,
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
