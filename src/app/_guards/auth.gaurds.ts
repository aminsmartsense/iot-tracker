import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {SharedService} from "@services/shared.service";
import {RouteConstants} from "@constants/routes";

@Injectable()
export class AdminAuthGuard implements CanActivate {
  constructor(private router: Router, private _sharedService: SharedService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let activateRoute = true;
    const readURL = state.url.split("?")[0];
    if (this._sharedService.isLoggedIn()) {
      if (readURL !== "/" + RouteConstants.LOGIN) {
        activateRoute = true;
      } else {
        activateRoute = false;
        this.router.navigate(["/" + RouteConstants.DASHBOARD]);
      }
    } else if (state.url !== "/" + RouteConstants.LOGIN) {
      this.router.navigate(["/" + RouteConstants.LOGIN]);
      this._sharedService.setLoginRequired(false);
      activateRoute = false;
    }
    return activateRoute;
  }
}
