import { Injectable } from '@angular/core';
import {Observable} from "rxjs/index";
import {API} from "@constants/api";
import {APIManager} from "@services/apimanager.service";

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor(private _apiManager: APIManager) { }

  addDevice(params, showLoader: boolean = true): Observable<any> {
    return this._apiManager.postAPI(API.ADD_DEVICE, params, this._apiManager.HttpOptions, showLoader);
  }

  getDeviceList(params, showLoader: boolean = true) {
    return this._apiManager.postAPI(API.GET_DEVICE, params, this._apiManager.HttpOptions, showLoader);
  }

  getDeviceNeighbourList(params, showLoader: boolean = true) {
    return this._apiManager.postAPI(API.GET_DEVICE_NEIGHBOUR, params, this._apiManager.HttpOptions, showLoader);
  }
}
