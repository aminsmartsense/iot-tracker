export class Device {
  private _id: number;
  private _macAddress: string;
  private _name: string;
  private _date: number;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get macAddress(): string {
    return this._macAddress;
  }

  set macAddress(value: string) {
    this._macAddress = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get date(): number {
    return this._date;
  }

  set date(value: number) {
    this._date = value;
  }
}

export class DeviceNeighbour {
  private _id: number;
  private _zoneId: number;
  private _zoneMacAddress: string;
  private _zoneName: string;
  private _senderDeviceId: number;
  private _senderDeviceName: string;
  private _senderFirtName: string;
  private _senderLastName: string;
  private _senderDeviceMacAddress: string;
  private _deviceId: number;
  private _deviceName: string;
  private _deviceMacAddress: string;
  private _firstName: string;
  private _lastName: string;
  private _rssi: number;
  private _date: number;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get zoneId(): number {
    return this._zoneId;
  }

  set zoneId(value: number) {
    this._zoneId = value;
  }

  get zoneMacAddress(): string {
    return this._zoneMacAddress;
  }

  set zoneMacAddress(value: string) {
    this._zoneMacAddress = value;
  }

  get zoneName(): string {
    return this._zoneName;
  }

  set zoneName(value: string) {
    this._zoneName = value;
  }

  get senderDeviceId(): number {
    return this._senderDeviceId;
  }

  set senderDeviceId(value: number) {
    this._senderDeviceId = value;
  }

  get senderDeviceName(): string {
    return this._senderDeviceName;
  }

  set senderDeviceName(value: string) {
    this._senderDeviceName = value;
  }

  get senderFirtName(): string {
    return this._senderFirtName;
  }

  set senderFirtName(value: string) {
    this._senderFirtName = value;
  }

  get senderLastName(): string {
    return this._senderLastName;
  }

  set senderLastName(value: string) {
    this._senderLastName = value;
  }

  get senderDeviceMacAddress(): string {
    return this._senderDeviceMacAddress;
  }

  set senderDeviceMacAddress(value: string) {
    this._senderDeviceMacAddress = value;
  }

  get deviceId(): number {
    return this._deviceId;
  }

  set deviceId(value: number) {
    this._deviceId = value;
  }

  get deviceName(): string {
    return this._deviceName;
  }

  set deviceName(value: string) {
    this._deviceName = value;
  }

  get deviceMacAddress(): string {
    return this._deviceMacAddress;
  }

  set deviceMacAddress(value: string) {
    this._deviceMacAddress = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get rssi(): number {
    return this._rssi;
  }

  set rssi(value: number) {
    this._rssi = value;
  }

  get date(): number {
    return this._date;
  }

  set date(value: number) {
    this._date = value;
  }
}
