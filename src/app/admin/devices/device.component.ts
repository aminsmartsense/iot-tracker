import {Component, HostListener, OnInit} from "@angular/core";
import {BaseComponent} from "../../utility/base-component/base.component";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CommonRegexp, ValidationConstant} from "@constants/validations";
import {DeviceService} from "./device.service";
import {AppConstant} from "@constants/base-constants";
import {Device} from "./device.model";
import {RouteConstants} from "@constants/routes";
import {Router} from "@angular/router";
import {SharedService} from "@services/shared.service";

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html'
})

export class DeviceComponent extends BaseComponent implements OnInit {


  // FormGroup Variable
  addDeviceForm: FormGroup;

  // validations message
  validationMsg = new ValidationConstant();

  // Data variable
  deviceList: Device[] = [];

  // pagination Data
  pageArray = AppConstant.PAGINATION_ARRAY;
  pageSize = AppConstant.PAGINATION_ARRAY[0];
  pageDefaultSize = AppConstant.FIRST_PAGE_LENGTH;
  page: number;
  totalRecords: number;
  loadingMessage = AppConstant.LOADING_DATA;

  // state variables
  isAddDevice = false;

  constructor(private _deviceService: DeviceService, private router: Router, private _sharedService: SharedService) {
    super();
  }

  ngOnInit() {
    this.initializeMethod();
  }

  initializeMethod() {
    this.createDeviceForm();
    this.getDeviceList(0);
  }

  /**
   * Create zone form
   */
  createDeviceForm() {
    this.addDeviceForm = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern(CommonRegexp.ALPHANUMERIC_SPACE_REGEXP),
        Validators.minLength(ValidationConstant.DEVICE_MIN_LENGTH),
        Validators.maxLength(ValidationConstant.DEVICE_MAX_LENGTH)
      ]),
      macAddress: new FormControl('', [Validators.required])
    });
  }

  /**
   * device list API
   * @param pageNumber
   */
  getDeviceList(pageNumber: number) {
    this.loadingMessage = AppConstant.LOADING_DATA;
    this._deviceService.getDeviceList(this.queryParams(pageNumber)).subscribe(response => {
      this.handleDeviceResponse(response);
    });
  }

  handleDeviceResponse(response: any) {
    this.deviceList = response.payload.content;
    this.loadingMessage = AppConstant.NO_DATA;
    this.page = response.payload.number;
    this.totalRecords = +response.payload.totalElements;

  }

  /**
   * On Device add form submit
   * @param value
   * @param valid
   */
  onDeviceSubmit(formValue: any, valid: boolean) {
    if (valid) {
      this._deviceService.addDevice(formValue).subscribe(response => {
        this.isAddDevice = false
        this.getDeviceList(0);
      });
    }
  }

  onPageChange(event: any) {
    this.pageSize = event.pageSize;
    this.getDeviceList(event.pageIndex);
  }

  /**
   * On Open modal
   * @param id
   */
  onShowAddDevice(id) {
    this.addDeviceForm.reset();
    this.isAddDevice = true;
    this.onOpenModal(id);
  }

  /**
   * On device details route
   */
  onDeviceDetails(deviceData: Device) {
    this._sharedService.setButtonData('device', deviceData);
    this.router.navigate(['/' + RouteConstants.DEVICE_DETAILS]);
  }

  /**
   * On close modal
   */
  onCloseModal() {
    this.addDeviceForm.reset();
    this.onRemoveScroll();
    this.isAddDevice = false;
  }

  /*Helpers*/
  @HostListener('window:keydown', ['$event'])
  keyboardInput(event: any) {
    if (event.keyCode === 27) {
      this.onCloseModal();
    }
  }

  // Helper
  queryParams(pageNumber: number): any {
    return {
      page: pageNumber,
      size: this.pageSize
    };
  }

}
