import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {RouteConstants} from "@constants/routes";
import {Device, DeviceNeighbour} from "../device.model";
import {AppConstant} from "@constants/base-constants";
import {SharedService} from "@services/shared.service";
import {ZonesService} from "../../zones/zones.service";
import {DeviceService} from "../device.service";

@Component({
  selector: 'app-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.scss'],
  providers: [DeviceService]
})
export class DeviceDetailsComponent implements OnInit {

  // pagination Data
  pageArray = AppConstant.PAGINATION_ARRAY;
  pageSize = AppConstant.PAGINATION_ARRAY[0];
  pageDefaultSize = AppConstant.FIRST_PAGE_LENGTH;
  page: number;
  totalRecords: number;
  loadingMessage = AppConstant.LOADING_DATA;

  selectedDevice: Device = null;

  deviceNeightbourList: DeviceNeighbour[] = [];

  constructor(private route: Router, private _sharedService: SharedService, private _deviceService: DeviceService) {
  }

  ngOnInit() {
    this.selectedDevice = this._sharedService.getButtonData('device');
    this.getDeviceDetailList(0);
  }

  getDeviceDetailList(pageNumber: number) {
    this.loadingMessage = AppConstant.LOADING_DATA;
    const criteria = [{'column': 'deviceId', 'values': [this.selectedDevice.id], 'operator': 3}];
    this._deviceService.getDeviceNeighbourList(this.queryParams(pageNumber, criteria)).subscribe(response => {
      this.handleZoneResponse(response);
    });
  }

  handleZoneResponse(response: any) {
    this.deviceNeightbourList = response.payload.content;
    this.loadingMessage = AppConstant.NO_DATA;
    this.page = response.payload.number;
    this.totalRecords = +response.payload.totalElements;
  }

  /**
   * On Device page route
   */
  onDevice(){
    this.route.navigate(['/' + RouteConstants.DEVICES]);
  }


  onPageChange(event: any) {
    this.pageSize = event.pageSize;
    this.getDeviceDetailList(event.pageIndex);
  }

  getDeviceDate(date: number) {
    return (new Date(date));
  }

  // Helper
  queryParams(pageNumber: number, criteria: any): any {
    return {
      page: pageNumber,
      size: this.pageSize,
      criteria: criteria
    };
  }



}
