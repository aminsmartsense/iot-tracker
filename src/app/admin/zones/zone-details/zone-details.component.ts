import {Component, OnDestroy, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {RouteConstants} from "@constants/routes";
import {ZoneNeighbour, Zones} from "../zones.module";
import {SharedService} from "@services/shared.service";
import {AppConstant} from "@constants/base-constants";
import {ZonesService} from "../zones.service";
import "rxjs/add/observable/interval";
import {Observable} from "rxjs/Rx";

@Component({
  selector: 'app-zone-details',
  templateUrl: './zone-details.component.html',
  styleUrls: ['./zone-details.component.scss']
})
export class ZoneDetailsComponent implements OnInit, OnDestroy {

  // pagination Data
  pageArray = AppConstant.PAGINATION_ARRAY;
  pageSize = AppConstant.PAGINATION_ARRAY[2];
  pageDefaultSize = AppConstant.FIRST_PAGE_LENGTH_NEIGHBOUR;
  page: number;
  totalRecords: number;
  loadingMessage = AppConstant.LOADING_DATA;

  selectedZone: Zones = null;

  zoneNeightbourList: ZoneNeighbour[] = [];

  sub: any;

  constructor(private route: Router, private _sharedService: SharedService, private _zonesService: ZonesService) {
  }

  ngOnInit() {
    this.selectedZone = this._sharedService.getButtonData('zone');
    this.getZoneDetailList(0);
    this.sub = Observable.interval(30000)
      .subscribe((val) => {
        this.getZoneDetailList(0);
      });
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
      this.sub = null;
    }
  }


  getZoneDetailList(pageNumber: number) {
    this.loadingMessage = AppConstant.LOADING_DATA;
    const criteria = [{'column': 'zoneId', 'values': [this.selectedZone.id], 'operator': 3}];
    this._zonesService.getZoneNeighbourList(this.queryParams(pageNumber, criteria)).subscribe(response => {
      this.handleZoneResponse(response);
    });
  }

  handleZoneResponse(response: any) {
    this.zoneNeightbourList = response.payload.content;
    this.loadingMessage = AppConstant.NO_DATA;
    this.page = response.payload.number;
    this.totalRecords = +response.payload.totalElements;
  }

  /**
   * On zone page route
   */
  onZone() {
    this.route.navigate(['/' + RouteConstants.ZONES]);
  }


  onPageChange(event: any) {
    this.pageSize = event.pageSize;
    this.getZoneDetailList(event.pageIndex);
  }

  getZoneDate(date: number) {
    return (new Date(date));
  }

  // Helper
  queryParams(pageNumber: number, criteria: any): any {
    return {
      page: pageNumber,
      size: this.pageSize,
      sort: {'column': 'date', 'sortType': 2},
      criteria: criteria
    };
  }
}
