import { Injectable } from '@angular/core';
import {Observable} from "rxjs/index";
import {APIManager} from "@services/apimanager.service";
import {API} from "@constants/api";

@Injectable({
  providedIn: 'root'
})
export class ZonesService {

  constructor(private _apiManager: APIManager) { }

  addZone(params, showLoader: boolean = true): Observable<any> {
    return this._apiManager.postAPI(API.ADD_ZONE, params, this._apiManager.HttpOptions, showLoader);
  }

  getZoneList(params, showLoader: boolean = true) {
    return this._apiManager.postAPI(API.GET_ZONE, params, this._apiManager.HttpOptions, showLoader);
  }

  getZoneNeighbourList(params, showLoader: boolean = true) {
    return this._apiManager.postAPI(API.GET_ZONE_NEIGHBOUR, params, this._apiManager.HttpOptions, showLoader);
  }
}
