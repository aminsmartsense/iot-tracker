export class Zones {
  get count(): number {
    return this._count;
  }

  set count(value: number) {
    this._count = value;
  }

  private _id: number;
  private _name: string;
  private _date: number;
  private _count: number;
  private _macAddress: string;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get date(): number {
    return this._date;
  }

  set date(value: number) {
    this._date = value;
  }

  get macAddress(): string {
    return this._macAddress;
  }

  set macAddress(value: string) {
    this._macAddress = value;
  }
}

export class ZoneNeighbour {
  private _date: number;
  private _deviceId: number;
  private _id: number;
  private _rssi: number;
  private _userId: number;
  private _zoneId: number;
  private _deviceMacAddress: string;
  private _zoneMacAddress: string;
  private _zoneName: string;
  private _deviceName: string;
  private _firstName: string;
  private _lastName: string;

  get date(): number {
    return this._date;
  }

  set date(value: number) {
    this._date = value;
  }

  get deviceId(): number {
    return this._deviceId;
  }

  set deviceId(value: number) {
    this._deviceId = value;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get rssi(): number {
    return this._rssi;
  }

  set rssi(value: number) {
    this._rssi = value;
  }

  get userId(): number {
    return this._userId;
  }

  set userId(value: number) {
    this._userId = value;
  }

  get zoneId(): number {
    return this._zoneId;
  }

  set zoneId(value: number) {
    this._zoneId = value;
  }

  get deviceMacAddress(): string {
    return this._deviceMacAddress;
  }

  set deviceMacAddress(value: string) {
    this._deviceMacAddress = value;
  }

  get zoneMacAddress(): string {
    return this._zoneMacAddress;
  }

  set zoneMacAddress(value: string) {
    this._zoneMacAddress = value;
  }

  get zoneName(): string {
    return this._zoneName;
  }

  set zoneName(value: string) {
    this._zoneName = value;
  }

  get deviceName(): string {
    return this._deviceName;
  }

  set deviceName(value: string) {
    this._deviceName = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }
}
