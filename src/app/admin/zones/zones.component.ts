import {Component, HostListener, OnInit} from "@angular/core";
import {BaseComponent} from "../../utility/base-component/base.component";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CommonRegexp, ValidationConstant} from "../../utility/constants/validations";
import {ZonesService} from "./zones.service";
import {Zones} from "./zones.module";
import {AppConstant} from "@constants/base-constants";
import {Router} from "@angular/router";
import {RouteConstants} from "@constants/routes";
import {SharedService} from "@services/shared.service";

@Component({
  selector: 'app-zones',
  templateUrl: './zones.component.html',
  providers: [ZonesService]
})
export class ZonesComponent extends BaseComponent implements OnInit {

  // FormGroup Variable
  addZoneForm: FormGroup;

  // validations message
  validationMsg = new ValidationConstant();

  // data variable
  zoneList: Zones[] = [];

  // pagination Data
  pageArray = AppConstant.PAGINATION_ARRAY;
  pageSize = AppConstant.PAGINATION_ARRAY[0];
  pageDefaultSize = AppConstant.FIRST_PAGE_LENGTH;
  page: number;
  totalRecords: number;
  loadingMessage = AppConstant.LOADING_DATA;

  // state variables
  isAddZone = false;

  constructor(private _zonesService: ZonesService, private route: Router, private _sharedService: SharedService) {
    super();
  }

  ngOnInit() {
    this.initializeMethod();
  }

  initializeMethod() {
    this.createZoneForm();
    this.getZoneList(0);
  }

  /**
   * Create zone form
   */
  createZoneForm() {
    this.addZoneForm = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern(CommonRegexp.ALPHANUMERIC_SPACE_REGEXP),
        Validators.minLength(ValidationConstant.ZONE_MIN_LENGTH),
        Validators.maxLength(ValidationConstant.ZONE_MAX_LENGTH)
      ]),
      macAddress: new FormControl('', [Validators.required])
    });
  }

  /**
   * zone list API
   * @param pageNumber
   */
  getZoneList(pageNumber: number) {
    this.loadingMessage = AppConstant.LOADING_DATA;
    this._zonesService.getZoneList(this.queryParams(pageNumber)).subscribe(response => {
      this.handleZoneResponse(response);
    });
  }

  handleZoneResponse(response: any) {
    this.zoneList = response.payload.content;
    this.loadingMessage = AppConstant.NO_DATA;
    this.page = response.payload.number;
    this.totalRecords = +response.payload.totalElements;
  }

  /**
   * On zone add form submit
   * @param value
   * @param valid
   */
  onZoneSubmit(formValue: any, valid: boolean) {
    if (valid) {
      this._zonesService.addZone(formValue).subscribe(response => {
        this.isAddZone = false;
        this.getZoneList(0);
      });
    }
  }

  onPageChange(event: any) {
    this.pageSize = event.pageSize;
    this.getZoneList(event.pageIndex);
  }

  /**
   * On Open modal
   * @param id
   */
  onShowAddZone(id) {
    this.addZoneForm.reset();
    this.isAddZone = true;
    this.onOpenModal(id);
  }

  /**
   * On close modal
   */
  onCloseModal() {
    this.addZoneForm.reset();
    this.onRemoveScroll();
    this.isAddZone = false;
  }

  /**
   * on zone details route
   */
  onZoneDetails(zoneData: Zones) {
    this._sharedService.setButtonData('zone', zoneData);
    this.route.navigate(['/' + RouteConstants.ZONE_DETAILS]);
  }

  /*Helpers*/
  @HostListener('window:keydown', ['$event'])
  keyboardInput(event: any) {
    if (event.keyCode === 27) {
      this.onCloseModal();
    }
  }

  // Helper
  queryParams(pageNumber: number): any {
    return {
      page: pageNumber,
      size: this.pageSize
    };
  }

}
