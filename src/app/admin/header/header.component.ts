import { Component, OnInit } from '@angular/core';
import {RouteConstants} from "../../utility/constants/routes";
import {Router} from "@angular/router";
import {SharedService} from "../../utility/shared-service/shared.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  /*state variables*/
  isDropdown = false;

  constructor(private router: Router, private _sharedService : SharedService) { }

  ngOnInit() {
  }

  onLogout(){
    this._sharedService.logout();
    this.isDropdown = false;
    this.router.navigate(['/' + RouteConstants.LOGIN]);
  }

  /*Helpers*/
  get dashboardUrl(){
    return ['/' + RouteConstants.DASHBOARD];
  }

  get zonesUrl(){
    return ['/' + RouteConstants.ZONES];
  }

  get devicesUrl(){
    return ['/' + RouteConstants.DEVICES];
  }

  get usersUrl(){
    return ['/' + RouteConstants.USERS];
  }

}
