import {Component, OnInit} from "@angular/core";
import {RouteConstants} from "../../utility/constants/routes";
import {Router} from "@angular/router";
import {AppConstant} from "@constants/base-constants";
import {ZonesService} from "../zones/zones.service";
import {Zones} from "../zones/zones.module";
import {SharedService} from "@services/shared.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  providers: [ZonesService]
})
export class DashboardComponent implements OnInit {

  loadingMessage = AppConstant.LOADING_DATA;

  // data variable
  zoneList: Zones[] = [];


  constructor(private router: Router, private _zonesService: ZonesService, private _sharedService: SharedService) {
  }

  ngOnInit() {
    this.getZoneList(0);
  }

  /**
   * redirect to zone table.
   * @param zoneData
   */
  onZoneGrid(zoneData: Zones) {
    this._sharedService.setButtonData('zone', zoneData);
    this.router.navigate(['/' + RouteConstants.ZONE_DETAILS]);
  }

  /**
   * zone list API
   * @param pageNumber
   */
  getZoneList(pageNumber: number) {
    this.loadingMessage = AppConstant.LOADING_DATA;
    this._zonesService.getZoneList(this.queryParams(pageNumber)).subscribe(response => {
      this.handleZoneResponse(response);
    });
  }

  handleZoneResponse(response: any) {
    this.zoneList = response.payload.content;
    this.loadingMessage = AppConstant.NO_DATA;
  }

  // Helper
  queryParams(pageNumber: number): any {
    return {
      page: pageNumber,
      size: 100
    };
  }

}
