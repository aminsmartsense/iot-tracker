import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RouteConstants} from "../utility/constants/routes";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ZonesComponent} from "./zones/zones.component";
import {UsersListComponent} from "./users/users-list/users-list.component";
import {UsersDetailsComponent} from "./users/users-details/users-details.component";
import {HomeComponent} from "./home/home.component";
import {DeviceComponent} from "./devices/device.component";
import {AdminAuthGuard} from "../_guards/auth.gaurds";
import {DeviceDetailsComponent} from "./devices/device-details/device-details.component";
import {ZoneDetailsComponent} from "./zones/zone-details/zone-details.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: RouteConstants.DASHBOARD_ROUTE,
        component: DashboardComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: RouteConstants.ZONES_ROUTE,
        component: ZonesComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: RouteConstants.USERS_ROUTE,
        component: UsersListComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: RouteConstants.USERS_ADD_ROUTE,
        component: UsersDetailsComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: RouteConstants.DEVICES_ROUTE,
        component: DeviceComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: RouteConstants.DEVICES_DETAILS_ROUTE,
        component: DeviceDetailsComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: RouteConstants.ZONES_DETAILS_ROUTE,
        component: ZoneDetailsComponent,
        canActivate: [AdminAuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
