import {Component, OnInit, HostListener} from '@angular/core';
import {BaseComponent} from "../../../utility/base-component/base.component";
import {RouteConstants} from "../../../utility/constants/routes";
import {Router} from "@angular/router";
import {UsersService} from "../users.service";
import {AppConstant} from "@constants/base-constants";
import {Users} from "../users.model";

@Component({
  selector: 'app-users-list',
  templateUrl: 'users-list.component.html'
})
export class UsersListComponent extends BaseComponent implements OnInit {

   // Data variable
  userList: Users[] = [];

  // pagination Data
  pageArray = AppConstant.PAGINATION_ARRAY;
  pageSize = AppConstant.PAGINATION_ARRAY[0];
  pageDefaultSize = AppConstant.FIRST_PAGE_LENGTH;
  page: number;
  totalRecords: number;
  loadingMessage = AppConstant.LOADING_DATA;

  // State variables
  isDeleteModal = false;


  constructor(private router: Router, private _usersService: UsersService) {
    super();
  }

  ngOnInit() {
    this.getUserList(0);
  }

  getUserList(pageNumber: number){
    this.loadingMessage = AppConstant.LOADING_DATA;
    this._usersService.getUsersList(this.queryParams(pageNumber)).subscribe(response => {
      this.handleUsersResponse(response);
    });
  }

  handleUsersResponse(response: any){
    this.userList = response.payload.content;
    this.loadingMessage = AppConstant.NO_DATA;
    this.page = response.payload.number;
    this.totalRecords = +response.payload.totalElements;
  }
  /**
   * On Open modal
   * @param id
   */
  onShowDeleteModal(id) {
    this.isDeleteModal = true;
    this.onOpenModal(id);
  }

  /**
   * On add user
   */
  onAddUser() {
    this.router.navigate(['/' + RouteConstants.USERS_ADD]);
  }

  /**
   * On close modal
   */
  onCloseModal() {
    this.onRemoveScroll();
    this.isDeleteModal = false;
  }

  /**
   * page change event
   * @param event
   */
  onPageChange(event: any) {
    this.pageSize = event.pageSize;
    this.getUserList(event.pageIndex);
  }

  /*Helpers*/
  @HostListener('window:keydown', ['$event'])
  keyboardInput(event: any) {
    if (event.keyCode === 27) {
      this.onCloseModal();
    }
  }

  // Helper
  queryParams(pageNumber: number): any {
    return {
      page: pageNumber,
      size: this.pageSize
    };
  }
}
