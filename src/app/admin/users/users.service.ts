import { Injectable } from '@angular/core';
import {APIManager} from "@services/apimanager.service";
import {API} from "@constants/api";
import {Observable} from 'rxjs';
import {BehaviorSubject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private _apiManager: APIManager) { }

  getDeviceList(params, showLoader: boolean = true) {
    return this._apiManager.postAPI(API.GET_DEVICE, params, this._apiManager.HttpOptions, showLoader);
  }

  getZoneList(params, showLoader: boolean = true) {
    return this._apiManager.postAPI(API.GET_ZONE, params, this._apiManager.HttpOptions, showLoader);
  }

  getUsersList(params, showLoader: boolean = true) {
    return this._apiManager.postAPI(API.GET_USERS, params, this._apiManager.HttpOptions, showLoader);
  }

  addUsers(params, showLoader: boolean = true): Observable<any> {
    return this._apiManager.postAPI(API.ADD_USERS, params, this._apiManager.HttpOptions, showLoader);
  }
}
