export class Users{
  private _id: number;
  private _firstName: string;
  private _lastName: string;
  private _deviceId: number;
  private _deviceName: string;
  private _zoneId: number;
  private _zoneName: string;


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get deviceId(): number {
    return this._deviceId;
  }

  set deviceId(value: number) {
    this._deviceId = value;
  }

  get deviceName(): string {
    return this._deviceName;
  }

  set deviceName(value: string) {
    this._deviceName = value;
  }

  get zoneId(): number {
    return this._zoneId;
  }

  set zoneId(value: number) {
    this._zoneId = value;
  }

  get zoneName(): string {
    return this._zoneName;
  }

  set zoneName(value: string) {
    this._zoneName = value;
  }
}
