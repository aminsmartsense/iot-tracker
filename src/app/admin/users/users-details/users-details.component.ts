import {Component, OnInit} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {BaseComponent} from "../../../utility/base-component/base.component";
import {ValidationConstant, CommonRegexp} from "../../../utility/constants/validations";
import {Router} from "@angular/router";
import {RouteConstants} from "../../../utility/constants/routes";
import {UsersService} from "../users.service";
import {Device} from "../../devices/device.model";
import {Zones} from "../../zones/zones.module";

@Component({
  selector: 'app-users-details',
  templateUrl: './users-details.component.html',
  providers: [UsersService]
})
export class UsersDetailsComponent extends BaseComponent implements OnInit {

  // FormGroup Variable
  userForm: FormGroup;

  // validations message
  validationMsg = new ValidationConstant();

  // data variable
  deviceList: Device[] = [];
  zoneList: Zones[] = [];

  constructor(private router: Router, private _usersService: UsersService) {
    super();
  }

  ngOnInit() {
    this.initializeMethod();
    this.getDeviceList();
    this.getZoneList();
  }

  initializeMethod() {
    this.createUserForm();
  }

  /**
   * Create user form
   */
  createUserForm() {
    this.userForm = new FormGroup({
      firstName: new FormControl('', [
        Validators.required, Validators.pattern(CommonRegexp.ONLY_ALPHA_REGEXP),
        Validators.minLength(ValidationConstant.FIRST_NAME_MIN_LENGTH),
        Validators.maxLength(ValidationConstant.FIRST_NAME_MAX_LENGTH)
      ]),
      lastName: new FormControl('', [
        Validators.required, Validators.pattern(CommonRegexp.ONLY_ALPHA_REGEXP),
        Validators.minLength(ValidationConstant.LAST_NAME_MIN_LENGTH),
        Validators.maxLength(ValidationConstant.LAST_NAME_MAX_LENGTH)
      ]),
      date: new FormControl('', Validators.required),
      department: new FormControl(''),
      deviceId: new FormControl('', Validators.required),
      // contactNo: new FormControl(''),
      zoneId: new FormControl('', Validators.required),
      // bloodGroup: new FormControl(''),
    });
  }

  getDeviceList() {
    this._usersService.getDeviceList(this.queryParams(0)).subscribe(response => {
      this.deviceList = response.payload.content;
    });
  }

  getZoneList() {
    this._usersService.getZoneList(this.queryParams(0)).subscribe(response => {
      this.zoneList = response.payload.content;
    });
  }

  /**
   * On user form submit
   * @param value
   * @param valid
   */
  onUserSubmit(formValue: any, isValid: boolean) {
    if (isValid) {
      for (const key in formValue) {
        if (formValue[key] === null || formValue[key] === '') {
          delete formValue[key];
        }
      }
      this._usersService.addUsers(formValue).subscribe(response => {
        this.initializeMethod();
      });
    }
  }

  /**
   * On user grid
   */
  onUserGrid() {
    this.router.navigate(['/' + RouteConstants.USERS]);
  }

  /** helper function
   *
   * @param pageNumber
   * @returns {{page: number, size: any}}
   */
  queryParams(pageNumber: number): any {
    return {
      page: pageNumber,
      size: 200  // backend records all api is not done.
    };
  }
}
