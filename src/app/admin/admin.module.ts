import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AdminRoutingModule} from "./admin-routing.module";
import {HeaderComponent} from "./header/header.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ZonesComponent} from "./zones/zones.component";
import {UsersDetailsComponent} from "./users/users-details/users-details.component";
import {HomeComponent} from "./home/home.component";
import {UtilityModule} from "../utility/utility.module";
import {DeviceComponent} from "./devices/device.component";
import {UsersListComponent} from "./users/users-list/users-list.component";
import { DeviceDetailsComponent } from './devices/device-details/device-details.component';
import {ZoneDetailsComponent} from "./zones/zone-details/zone-details.component";


@NgModule({
  imports: [
    CommonModule,
    UtilityModule,
    AdminRoutingModule
  ],
  declarations: [
    HeaderComponent,
    DashboardComponent,
    ZonesComponent,
    UsersListComponent,
    UsersDetailsComponent,
    DeviceComponent,
    HomeComponent,
    DeviceDetailsComponent,
    ZoneDetailsComponent
  ],
  exports: [
    HeaderComponent
  ]
})
export class AdminModule {
}
