(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./admin/admin.module": [
		"./src/app/admin/admin.module.ts",
		"admin-admin-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var module = __webpack_require__(ids[0]);
		return module;
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/_guards/auth.gaurds.ts":
/*!****************************************!*\
  !*** ./src/app/_guards/auth.gaurds.ts ***!
  \****************************************/
/*! exports provided: AdminAuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminAuthGuard", function() { return AdminAuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @services/shared.service */ "./src/app/utility/shared-service/shared.service.ts");
/* harmony import */ var _constants_routes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @constants/routes */ "./src/app/utility/constants/routes.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminAuthGuard = /** @class */ (function () {
    function AdminAuthGuard(router, _sharedService) {
        this.router = router;
        this._sharedService = _sharedService;
    }
    AdminAuthGuard.prototype.canActivate = function (route, state) {
        var activateRoute = true;
        var readURL = state.url.split("?")[0];
        if (this._sharedService.isLoggedIn()) {
            if (readURL !== "/" + _constants_routes__WEBPACK_IMPORTED_MODULE_3__["RouteConstants"].LOGIN) {
                activateRoute = true;
            }
            else {
                activateRoute = false;
                this.router.navigate(["/" + _constants_routes__WEBPACK_IMPORTED_MODULE_3__["RouteConstants"].DASHBOARD]);
            }
        }
        else if (state.url !== "/" + _constants_routes__WEBPACK_IMPORTED_MODULE_3__["RouteConstants"].LOGIN) {
            this.router.navigate(["/" + _constants_routes__WEBPACK_IMPORTED_MODULE_3__["RouteConstants"].LOGIN]);
            this._sharedService.setLoginRequired(false);
            activateRoute = false;
        }
        return activateRoute;
    };
    AdminAuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_shared_service__WEBPACK_IMPORTED_MODULE_2__["SharedService"]])
    ], AdminAuthGuard);
    return AdminAuthGuard;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _constants_base_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @constants/base-constants */ "./src/app/utility/constants/base-constants.ts");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @services/shared.service */ "./src/app/utility/shared-service/shared.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = /** @class */ (function () {
    function AppComponent(toastr, _sharedService) {
        this.toastr = toastr;
        this._sharedService = _sharedService;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.configureToastManager();
    };
    AppComponent.prototype.configureToastManager = function () {
        var _this = this;
        var individualConfig = {};
        this.toastSubscriber$ = this._sharedService.getToastMessage().subscribe(function (msgBody) {
            if (msgBody) {
                if (msgBody.type === _constants_base_constants__WEBPACK_IMPORTED_MODULE_2__["ToastStatus"].SUCCESS) {
                    setTimeout(function () { return _this.toastr.success(msgBody.message, msgBody.title, _constants_base_constants__WEBPACK_IMPORTED_MODULE_2__["individualToastConfig"]); });
                }
                else if (msgBody.type === _constants_base_constants__WEBPACK_IMPORTED_MODULE_2__["ToastStatus"].ERROR) {
                    setTimeout(function () { return _this.toastr.error(msgBody.message, msgBody.title, _constants_base_constants__WEBPACK_IMPORTED_MODULE_2__["individualToastConfig"]); });
                }
                _this._sharedService.setToastMessage('', _constants_base_constants__WEBPACK_IMPORTED_MODULE_2__["ToastStatus"].UNKNOWN);
            }
        });
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html")
        }),
        __metadata("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"],
            _services_shared_service__WEBPACK_IMPORTED_MODULE_3__["SharedService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _user_auth_user_auth_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user-auth/user-auth.module */ "./src/app/user-auth/user-auth.module.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.routing.module */ "./src/app/app.routing.module.ts");
/* harmony import */ var _constants_base_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @constants/base-constants */ "./src/app/utility/constants/base-constants.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _guards_auth_gaurds__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./_guards/auth.gaurds */ "./src/app/_guards/auth.gaurds.ts");
/* harmony import */ var _utility_utility_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./utility/utility.module */ "./src/app/utility/utility.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["BrowserAnimationsModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrModule"].forRoot(_constants_base_constants__WEBPACK_IMPORTED_MODULE_5__["globalToastConfig"]),
                _user_auth_user_auth_module__WEBPACK_IMPORTED_MODULE_3__["UserAuthModule"],
                _utility_utility_module__WEBPACK_IMPORTED_MODULE_9__["UtilityModule"].forRoot(),
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
            ],
            providers: [
                _guards_auth_gaurds__WEBPACK_IMPORTED_MODULE_8__["AdminAuthGuard"]
            ],
            bootstrap: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]
            ]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app.routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _utility_constants_routes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utility/constants/routes */ "./src/app/utility/constants/routes.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: _utility_constants_routes__WEBPACK_IMPORTED_MODULE_2__["RouteConstants"].ADMIN,
        loadChildren: './admin/admin.module#AdminModule'
    },
    {
        path: '**',
        redirectTo: _utility_constants_routes__WEBPACK_IMPORTED_MODULE_2__["RouteConstants"].LOGIN,
        pathMatch: 'full',
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/user-auth/login/login.component.html":
/*!******************************************************!*\
  !*** ./src/app/user-auth/login/login.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-box\">\n  <div class=\"login-box__image\">\n    <img src=\"assets/images/logo.png\" alt=\"\">\n  </div>\n  <form [formGroup]=\"loginForm\" (submit)=\"onLoginFormSubmit(loginForm.value,loginForm.valid)\">\n    <div class=\"login-bg-white\">\n      <h1 class=\"primary-color text-center MB20\">Welcome!</h1>\n      <div class=\"login-box__body\">\n        <div class=\"basic-input basic-input-border no-label icons-group\">\n          <i class=\"fa fa-user\"></i>\n          <input formControlName=\"userName\" focus placeholder=\"Username\" type=\"text\">\n          <div class=\"validation-group\">\n            <validation *ngIf=\"isRequiredField(loginForm.get('userName'))\"\n                        [errMsg]=\"validationMsg.USERNAME_REQUIRED\"></validation>\n            <validation *ngIf=\"isValidField(loginForm.get('userName'))\"\n                        [errMsg]=\"validationMsg.USERNAME_NOT_VALID\"></validation>\n            <validation *ngIf=\"isValidLength(loginForm.get('userName'))\"\n                        [errMsg]=\"validationMsg.USERNAME_LENGTH\"></validation>\n          </div>\n        </div>\n        <div class=\"basic-input basic-input-border no-label icons-group\">\n          <i class=\"fa fa-lock\"></i>\n          <input formControlName=\"password\" placeholder=\"Password\" type=\"password\">\n          <div class=\"validation-group\">\n            <validation *ngIf=\"isRequiredField(loginForm.get('password'))\"\n                        [errMsg]=\"validationMsg.PASSWORD_REQUIRED\"></validation>\n            <validation *ngIf=\"isValidField(loginForm.get('password'))\"\n                        [errMsg]=\"validationMsg.PASSWORD_VALID\"></validation>\n            <validation *ngIf=\"isValidLength(loginForm.get('password'))\"\n                        [errMsg]=\"validationMsg.PASSWORD_LENGTH\"></validation>\n          </div>\n        </div>\n      </div>\n      <div class=\"login-box__footer\">\n        <div>\n          <button [disabled]=\"loginForm.invalid\" type=\"submit\" class=\"btn-primary btn btn-block\">Login</button>\n        </div>\n      </div>\n    </div>\n  </form>\n  <div class=\"text-center MT15\">\n    <span class=\"dark-grey-color copyright\">Copyright &copy; 2018 smartsensesolutions.com</span>\n  </div>\n</div>\n<div class=\"login-bg\"></div>\n"

/***/ }),

/***/ "./src/app/user-auth/login/login.component.scss":
/*!******************************************************!*\
  !*** ./src/app/user-auth/login/login.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user-auth/login/login.component.ts":
/*!****************************************************!*\
  !*** ./src/app/user-auth/login/login.component.ts ***!
  \****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _utility_constants_validations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../utility/constants/validations */ "./src/app/utility/constants/validations.ts");
/* harmony import */ var _utility_base_component_base_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../utility/base-component/base.component */ "./src/app/utility/base-component/base.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _utility_constants_routes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../utility/constants/routes */ "./src/app/utility/constants/routes.ts");
/* harmony import */ var _utility_shared_service_shared_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../utility/shared-service/shared.service */ "./src/app/utility/shared-service/shared.service.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login.service */ "./src/app/user-auth/login/login.service.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var LoginComponent = /** @class */ (function (_super) {
    __extends(LoginComponent, _super);
    function LoginComponent(router, _sharedService, _loginService) {
        var _this = _super.call(this) || this;
        _this.router = router;
        _this._sharedService = _sharedService;
        _this._loginService = _loginService;
        // validations message
        _this.validationMsg = new _utility_constants_validations__WEBPACK_IMPORTED_MODULE_2__["ValidationConstant"]();
        return _this;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.initializeMethods();
        //this._sharedService.setToastMessage('hey toast is working', ToastStatus.SUCCESS);
    };
    /**
     * Initialize method
     */
    LoginComponent.prototype.initializeMethods = function () {
        this.createLoginForm();
    };
    /**
     * Create login form
     */
    LoginComponent.prototype.createLoginForm = function () {
        this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            userName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(_utility_constants_validations__WEBPACK_IMPORTED_MODULE_2__["CommonRegexp"].ALPHA_NUMERIC_REGEXP),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(_utility_constants_validations__WEBPACK_IMPORTED_MODULE_2__["ValidationConstant"].USERNAME_MIN_LENGTH),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(_utility_constants_validations__WEBPACK_IMPORTED_MODULE_2__["ValidationConstant"].USERNAME_MAX_LENGTH)
            ]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(_utility_constants_validations__WEBPACK_IMPORTED_MODULE_2__["CommonRegexp"].PASSWORD_REGEXP),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(_utility_constants_validations__WEBPACK_IMPORTED_MODULE_2__["ValidationConstant"].PASSWORD_MIN_LENGTH)
            ])
        });
    };
    /**
     * On Login form submit
     * @param value
     * @param valid
     */
    LoginComponent.prototype.onLoginFormSubmit = function (value, valid) {
        var _this = this;
        if (valid) {
            this._loginService.login(value).subscribe(function (response) {
                _this._sharedService.setLoginRequired(true);
                _this.router.navigate(['/' + _utility_constants_routes__WEBPACK_IMPORTED_MODULE_5__["RouteConstants"].DASHBOARD]);
            });
        }
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/user-auth/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/user-auth/login/login.component.scss")],
            providers: [_login_service__WEBPACK_IMPORTED_MODULE_7__["LoginService"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _utility_shared_service_shared_service__WEBPACK_IMPORTED_MODULE_6__["SharedService"], _login_service__WEBPACK_IMPORTED_MODULE_7__["LoginService"]])
    ], LoginComponent);
    return LoginComponent;
}(_utility_base_component_base_component__WEBPACK_IMPORTED_MODULE_3__["BaseComponent"]));



/***/ }),

/***/ "./src/app/user-auth/login/login.service.ts":
/*!**************************************************!*\
  !*** ./src/app/user-auth/login/login.service.ts ***!
  \**************************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_apimanager_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @services/apimanager.service */ "./src/app/utility/shared-service/apimanager.service.ts");
/* harmony import */ var _constants_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @constants/api */ "./src/app/utility/constants/api.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginService = /** @class */ (function () {
    function LoginService(_apiManager) {
        this._apiManager = _apiManager;
    }
    LoginService.prototype.login = function (params) {
        return this._apiManager.postAPI(_constants_api__WEBPACK_IMPORTED_MODULE_2__["API"].LOGIN, params, this._apiManager.HttpOptions_2, true, false);
    };
    LoginService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_services_apimanager_service__WEBPACK_IMPORTED_MODULE_1__["APIManager"]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/user-auth/user-auth-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/user-auth/user-auth-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: UserAuthRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserAuthRoutingModule", function() { return UserAuthRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _utility_constants_routes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utility/constants/routes */ "./src/app/utility/constants/routes.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/user-auth/login/login.component.ts");
/* harmony import */ var _guards_auth_gaurds__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_guards/auth.gaurds */ "./src/app/_guards/auth.gaurds.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: _utility_constants_routes__WEBPACK_IMPORTED_MODULE_2__["RouteConstants"].LOGIN,
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"],
        canActivate: [_guards_auth_gaurds__WEBPACK_IMPORTED_MODULE_4__["AdminAuthGuard"]]
    }
];
var UserAuthRoutingModule = /** @class */ (function () {
    function UserAuthRoutingModule() {
    }
    UserAuthRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], UserAuthRoutingModule);
    return UserAuthRoutingModule;
}());



/***/ }),

/***/ "./src/app/user-auth/user-auth.module.ts":
/*!***********************************************!*\
  !*** ./src/app/user-auth/user-auth.module.ts ***!
  \***********************************************/
/*! exports provided: UserAuthModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserAuthModule", function() { return UserAuthModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _user_auth_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user-auth-routing.module */ "./src/app/user-auth/user-auth-routing.module.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/user-auth/login/login.component.ts");
/* harmony import */ var _utility_utility_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utility/utility.module */ "./src/app/utility/utility.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var UserAuthModule = /** @class */ (function () {
    function UserAuthModule() {
    }
    UserAuthModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _utility_utility_module__WEBPACK_IMPORTED_MODULE_4__["UtilityModule"],
                _user_auth_routing_module__WEBPACK_IMPORTED_MODULE_2__["UserAuthRoutingModule"]
            ],
            declarations: [
                _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"]
            ]
        })
    ], UserAuthModule);
    return UserAuthModule;
}());



/***/ }),

/***/ "./src/app/utility/base-component/base.component.html":
/*!************************************************************!*\
  !*** ./src/app/utility/base-component/base.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n"

/***/ }),

/***/ "./src/app/utility/base-component/base.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/utility/base-component/base.component.ts ***!
  \**********************************************************/
/*! exports provided: BaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseComponent", function() { return BaseComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants_base_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../constants/base-constants */ "./src/app/utility/constants/base-constants.ts");
/* harmony import */ var _shared_functions_common_functions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared-functions/common-functions */ "./src/app/utility/shared-functions/common-functions.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BaseComponent = /** @class */ (function () {
    function BaseComponent() {
        this.isCopied = false;
        this.type = 'basic';
    }
    BaseComponent.prototype.ngOnInit = function () {
    };
    // Form Validation Methods
    BaseComponent.prototype.isRequiredField = function (formControlName) {
        return formControlName.hasError('required') && formControlName.touched;
    };
    BaseComponent.prototype.isValidField = function (formControlName) {
        return formControlName.hasError('pattern');
    };
    BaseComponent.prototype.isValidLength = function (formControlName) {
        return formControlName.hasError('minlength') || formControlName.hasError('maxlength');
    };
    BaseComponent.prototype.hasError = function (errorName, formGroup, formControl) {
        return formGroup.hasError(errorName) && formControl.touched;
    };
    BaseComponent.prototype.isRequiredFieldCheck = function (currentControl, controlToCheck) {
        return (!currentControl.value) && controlToCheck.touched;
    };
    // scroll function
    BaseComponent.prototype.onScroll = function () {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    };
    // get html code
    BaseComponent.prototype.getHtmlCode = function (className) {
        return this.querySelector(className);
    };
    BaseComponent.prototype.querySelector = function (className) {
        return document.querySelectorAll('.' + className)[0]['outerHTML'];
    };
    BaseComponent.prototype.onCopySingle = function (val) {
        var _this = this;
        this.isCopied = Object(_shared_functions_common_functions__WEBPACK_IMPORTED_MODULE_2__["ClipboardText"])(val);
        setTimeout(function () {
            _this.isCopied = false;
        }, 1000);
    };
    BaseComponent.prototype.onCopyFile = function (file) {
        this.onCopySingle(file);
    };
    // Modal vertically center
    BaseComponent.prototype.onOpenModal = function (id) {
        Object(_shared_functions_common_functions__WEBPACK_IMPORTED_MODULE_2__["SetupScrollClass"])(_constants_base_constants__WEBPACK_IMPORTED_MODULE_1__["PopupState"].Open);
        setTimeout(function () {
            document.getElementById(id).style.top = ((window.innerHeight) / 2) - ((document.getElementById(id).offsetHeight) / 2) + 'px';
            document.getElementById(id).style.opacity = '1';
        }, 5);
    };
    // Hide body scroll
    BaseComponent.prototype.onRemoveScroll = function () {
        Object(_shared_functions_common_functions__WEBPACK_IMPORTED_MODULE_2__["SetupScrollClass"])(_constants_base_constants__WEBPACK_IMPORTED_MODULE_1__["PopupState"].Close);
    };
    BaseComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-base-component',
            template: __webpack_require__(/*! ./base.component.html */ "./src/app/utility/base-component/base.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], BaseComponent);
    return BaseComponent;
}());



/***/ }),

/***/ "./src/app/utility/common-functions.ts":
/*!*********************************************!*\
  !*** ./src/app/utility/common-functions.ts ***!
  \*********************************************/
/*! exports provided: CommonFunctions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonFunctions", function() { return CommonFunctions; });
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _constants_base_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @constants/base-constants */ "./src/app/utility/constants/base-constants.ts");


var CommonFunctions = /** @class */ (function () {
    function CommonFunctions() {
    }
    CommonFunctions.ENCRYPT_OBJ = function (value) {
        return crypto_js__WEBPACK_IMPORTED_MODULE_0__["AES"].encrypt(JSON.stringify(value), _constants_base_constants__WEBPACK_IMPORTED_MODULE_1__["BASE"].ENCRYPTION_TOKEN);
    };
    CommonFunctions.DECRYPT_OBJ = function (value) {
        if (value && value != null) {
            var bytes = crypto_js__WEBPACK_IMPORTED_MODULE_0__["AES"].decrypt(value.toString(), _constants_base_constants__WEBPACK_IMPORTED_MODULE_1__["BASE"].ENCRYPTION_TOKEN);
            var decryptedData = JSON.parse(bytes.toString(crypto_js__WEBPACK_IMPORTED_MODULE_0__["enc"].Utf8));
            return decryptedData;
        }
        return '';
    };
    return CommonFunctions;
}());



/***/ }),

/***/ "./src/app/utility/constants/api.ts":
/*!******************************************!*\
  !*** ./src/app/utility/constants/api.ts ***!
  \******************************************/
/*! exports provided: API */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API", function() { return API; });
/* harmony import */ var _base_constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base-constants */ "./src/app/utility/constants/base-constants.ts");

var API = /** @class */ (function () {
    function API() {
    }
    API.LOGIN = _base_constants__WEBPACK_IMPORTED_MODULE_0__["BASE"].API_URL + 'login';
    API.ADD_DEVICE = _base_constants__WEBPACK_IMPORTED_MODULE_0__["BASE"].API_URL + 'api/device';
    API.GET_DEVICE = _base_constants__WEBPACK_IMPORTED_MODULE_0__["BASE"].API_URL + 'api/device/all';
    API.GET_ZONE = _base_constants__WEBPACK_IMPORTED_MODULE_0__["BASE"].API_URL + 'api/zone/all';
    API.ADD_ZONE = _base_constants__WEBPACK_IMPORTED_MODULE_0__["BASE"].API_URL + 'api/zone';
    API.GET_USERS = _base_constants__WEBPACK_IMPORTED_MODULE_0__["BASE"].API_URL + 'api/user/all';
    API.ADD_USERS = _base_constants__WEBPACK_IMPORTED_MODULE_0__["BASE"].API_URL + 'api/user';
    API.GET_ZONE_NEIGHBOUR = _base_constants__WEBPACK_IMPORTED_MODULE_0__["BASE"].API_URL + 'api/gatewayneighbour';
    API.GET_DEVICE_NEIGHBOUR = _base_constants__WEBPACK_IMPORTED_MODULE_0__["BASE"].API_URL + 'api/deviceneighbour';
    return API;
}());



/***/ }),

/***/ "./src/app/utility/constants/base-constants.ts":
/*!*****************************************************!*\
  !*** ./src/app/utility/constants/base-constants.ts ***!
  \*****************************************************/
/*! exports provided: BASE, HttpStatus, ToastStatus, AppConstant, globalToastConfig, individualToastConfig, PopupState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BASE", function() { return BASE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpStatus", function() { return HttpStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastStatus", function() { return ToastStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppConstant", function() { return AppConstant; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "globalToastConfig", function() { return globalToastConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "individualToastConfig", function() { return individualToastConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopupState", function() { return PopupState; });
var BASE = /** @class */ (function () {
    function BASE() {
    }
    BASE.TOAST_TIMEOUT = 3000;
    BASE.URL = 'http://52.70.41.18:8093/';
    BASE.API_URL = BASE.URL;
    BASE.ENCRYPTION_TOKEN = 'smartsensebutton';
    return BASE;
}());

var HttpStatus = /** @class */ (function () {
    function HttpStatus() {
    }
    HttpStatus.SUCCESS = 200;
    HttpStatus.UNAUTHORIZED = 401;
    HttpStatus.EXPIRED = 450;
    return HttpStatus;
}());

var ToastStatus;
(function (ToastStatus) {
    ToastStatus[ToastStatus["UNKNOWN"] = 0] = "UNKNOWN";
    ToastStatus[ToastStatus["SUCCESS"] = 1] = "SUCCESS";
    ToastStatus[ToastStatus["ERROR"] = 2] = "ERROR";
    ToastStatus[ToastStatus["MULTIPLE"] = 3] = "MULTIPLE";
})(ToastStatus || (ToastStatus = {}));
var AppConstant = /** @class */ (function () {
    function AppConstant() {
    }
    AppConstant.PAGE_SIZE = 20;
    AppConstant.LOADING_DATA = 'Loading...';
    AppConstant.NO_DATA = 'No data found';
    AppConstant.PAGINATION_ARRAY = [10, 25, 50, 100];
    AppConstant.FIVE_MB_IMAGE_SIZE = 5000000;
    AppConstant.FIRST_PAGE_LENGTH = 10;
    AppConstant.FIRST_PAGE_LENGTH_NEIGHBOUR = 50;
    return AppConstant;
}());

var globalToastConfig = {
    positionClass: 'toast-top-right',
    maxOpened: 1,
    preventDuplicates: true
};
// IndividualConfig
var individualToastConfig = {
    timeOut: BASE.TOAST_TIMEOUT,
    closeButton: true,
};
var PopupState;
(function (PopupState) {
    PopupState[PopupState["Open"] = 0] = "Open";
    PopupState[PopupState["Close"] = 1] = "Close";
})(PopupState || (PopupState = {}));


/***/ }),

/***/ "./src/app/utility/constants/routes.ts":
/*!*********************************************!*\
  !*** ./src/app/utility/constants/routes.ts ***!
  \*********************************************/
/*! exports provided: RouteConstants */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouteConstants", function() { return RouteConstants; });
var RouteConstants = /** @class */ (function () {
    function RouteConstants() {
    }
    RouteConstants.ADMIN = 'admin';
    RouteConstants.LOGIN = 'login';
    /*For library*/
    RouteConstants.DASHBOARD_ROUTE = 'dashboard';
    RouteConstants.ZONES_ROUTE = 'zones';
    RouteConstants.ZONES_DETAILS_ROUTE = 'zone/details';
    RouteConstants.USERS_ROUTE = 'users';
    RouteConstants.USERS_ADD_ROUTE = 'users/add';
    RouteConstants.DEVICES_ROUTE = 'devices';
    RouteConstants.DEVICES_DETAILS_ROUTE = 'device/details';
    RouteConstants.DASHBOARD = '/' + RouteConstants.ADMIN + '/' + RouteConstants.DASHBOARD_ROUTE;
    RouteConstants.ZONES = '/' + RouteConstants.ADMIN + '/' + RouteConstants.ZONES_ROUTE;
    RouteConstants.ZONE_DETAILS = '/' + RouteConstants.ADMIN + '/' + RouteConstants.ZONES_DETAILS_ROUTE;
    RouteConstants.USERS = '/' + RouteConstants.ADMIN + '/' + RouteConstants.USERS_ROUTE;
    RouteConstants.USERS_ADD = '/' + RouteConstants.ADMIN + '/' + RouteConstants.USERS_ADD_ROUTE;
    RouteConstants.DEVICES = '/' + RouteConstants.ADMIN + '/' + RouteConstants.DEVICES_ROUTE;
    RouteConstants.DEVICE_DETAILS = '/' + RouteConstants.ADMIN + '/' + RouteConstants.DEVICES_DETAILS_ROUTE;
    return RouteConstants;
}());



/***/ }),

/***/ "./src/app/utility/constants/storage.ts":
/*!**********************************************!*\
  !*** ./src/app/utility/constants/storage.ts ***!
  \**********************************************/
/*! exports provided: APPStorage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APPStorage", function() { return APPStorage; });
var APPStorage = /** @class */ (function () {
    function APPStorage() {
    }
    APPStorage.TOKEN = 'at';
    APPStorage.USER = 'u';
    APPStorage.BUTTONDETAIL = 'BD';
    return APPStorage;
}());



/***/ }),

/***/ "./src/app/utility/constants/validations.ts":
/*!**************************************************!*\
  !*** ./src/app/utility/constants/validations.ts ***!
  \**************************************************/
/*! exports provided: CommonRegexp, ValidationConstant */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonRegexp", function() { return CommonRegexp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidationConstant", function() { return ValidationConstant; });
var CommonRegexp = /** @class */ (function () {
    function CommonRegexp() {
    }
    CommonRegexp.NUMERIC_REGEXP = '^[0-9]*$';
    CommonRegexp.ONLY_ALPHA_REGEXP = '^[a-zA-Z]*$';
    CommonRegexp.ALPHA_NUMERIC_REGEXP = '^[A-Za-z0-9]*$';
    CommonRegexp.USER_NAME_REGEXP = '^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z\\d#?.!@$%^&*-]+$';
    CommonRegexp.EMAIL_ADDRESS_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    CommonRegexp.PASSWORD_REGEXP = /^(?=.*[a-zA-Z])(?=.*[A-Za-z])(?=.*\d)[a-zA-Z\d#?!@$%^&*-]{8,}$/;
    CommonRegexp.MAC_ADDRESS_REGEXP = /^(([A-Fa-f0-9]{2}[:]){7}[A-Fa-f0-9]{2}[,]?)+$/;
    CommonRegexp.ALPHANUMERIC_SPACE_REGEXP = '^[a-zA-Z0-9 ]*$';
    return CommonRegexp;
}());

var ValidationConstant = /** @class */ (function () {
    function ValidationConstant() {
        this.IS_REQUIRED = ' is required';
        this.USERNAME = 'UserName' + this.IS_REQUIRED.replace('is', 'are');
        this.VALID_EMAIL = 'Enter valid email address';
        this.VALID_DOCUMENT_TYPE = 'Valid Document type is PDF';
        this.VALID_IMAGE_TYPE = 'Valid Image types are png, jpg, jpeg';
        this.EMAIL_REQUIRED = 'Email address' + this.IS_REQUIRED;
        this.EMAIL_VALID = 'Enter valid email address';
        this.EMAIL_LENGTH = 'email address length between' + ValidationConstant.EMAIL_MIN_LENGTH + ' to ' + ValidationConstant.EMAIL_MAX_LENGTH;
        this.PASSWORD_REQUIRED = 'Password' + this.IS_REQUIRED;
        this.PASSWORD_VALID = 'Enter valid password';
        this.PASSWORD_LENGTH = 'Password minimum length is ' + ValidationConstant.PASSWORD_MIN_LENGTH;
        this.USERNAME_REQUIRED = 'Username' + this.IS_REQUIRED;
        this.USERNAME_LENGTH = 'Username length between ' + ValidationConstant.USERNAME_MIN_LENGTH + ' to ' + ValidationConstant.USERNAME_MAX_LENGTH;
        this.USERNAME_NOT_VALID = 'Username is not valid';
        this.ZONE_NAME_LENGTH = 'Zone name length between ' + ValidationConstant.ZONE_MIN_LENGTH + ' to ' + ValidationConstant.ZONE_MAX_LENGTH;
        this.ZONE_NAME_VALID = 'Enter valid zone name';
        this.DEVICE_NAME_LENGTH = 'Device name length between ' + ValidationConstant.DEVICE_MIN_LENGTH + ' to ' + ValidationConstant.DEVICE_MAX_LENGTH;
        this.DEVICE_NAME_VALID = 'Enter valid device name';
        this.NAME_REQUIRED = 'Name' + this.IS_REQUIRED;
        this.FIRST_NAME_REQUIRED = 'First name' + this.IS_REQUIRED;
        this.FIRST_NAME_LENGTH = 'First name length between ' + ValidationConstant.FIRST_NAME_MIN_LENGTH + ' to ' + ValidationConstant.FIRST_NAME_MAX_LENGTH;
        this.FIRST_NAME_VALID = 'Enter valid first name';
        this.LAST_NAME_REQUIRED = 'Last name' + this.IS_REQUIRED;
        this.LAST_NAME_LENGTH = 'Last name length between ' + ValidationConstant.LAST_NAME_MIN_LENGTH + ' to ' + ValidationConstant.LAST_NAME_MAX_LENGTH;
        this.LAST_NAME_VALID = 'Enter valid last name';
        this.MAC_ADDRESS_REQUIRED = 'Mac address' + this.IS_REQUIRED;
        this.MAC_ADDRESS_VALID = 'Enter valid Mac address';
        this.DATE_REQUIRED = 'Date' + this.IS_REQUIRED;
    }
    /*Email*/
    ValidationConstant.EMAIL_MIN_LENGTH = 6;
    ValidationConstant.EMAIL_MAX_LENGTH = 40;
    ValidationConstant.USERNAME_MIN_LENGTH = 2;
    ValidationConstant.USERNAME_MAX_LENGTH = 30;
    ValidationConstant.ZONE_MIN_LENGTH = 2;
    ValidationConstant.ZONE_MAX_LENGTH = 30;
    ValidationConstant.DEVICE_MIN_LENGTH = 2;
    ValidationConstant.DEVICE_MAX_LENGTH = 30;
    ValidationConstant.FIRST_NAME_MIN_LENGTH = 2;
    ValidationConstant.FIRST_NAME_MAX_LENGTH = 30;
    ValidationConstant.LAST_NAME_MIN_LENGTH = 2;
    ValidationConstant.LAST_NAME_MAX_LENGTH = 30;
    /*Password*/
    ValidationConstant.PASSWORD_MIN_LENGTH = 8;
    return ValidationConstant;
}());



/***/ }),

/***/ "./src/app/utility/directives/disable-control.directive.ts":
/*!*****************************************************************!*\
  !*** ./src/app/utility/directives/disable-control.directive.ts ***!
  \*****************************************************************/
/*! exports provided: DisableControlDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisableControlDirective", function() { return DisableControlDirective; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DisableControlDirective = /** @class */ (function () {
    function DisableControlDirective(ngControl) {
        this.ngControl = ngControl;
    }
    Object.defineProperty(DisableControlDirective.prototype, "disableControl", {
        set: function (condition) {
            var action = condition ? 'disable' : 'enable';
            this.ngControl.control[action]();
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], DisableControlDirective.prototype, "disableControl", null);
    DisableControlDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[disableControl]'
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControl"]])
    ], DisableControlDirective);
    return DisableControlDirective;
}());



/***/ }),

/***/ "./src/app/utility/directives/focus.directive.ts":
/*!*******************************************************!*\
  !*** ./src/app/utility/directives/focus.directive.ts ***!
  \*******************************************************/
/*! exports provided: FocusDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FocusDirective", function() { return FocusDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FocusDirective = /** @class */ (function () {
    function FocusDirective(elementRef) {
        this.elementRef = elementRef;
        this.el = this.elementRef.nativeElement;
    }
    FocusDirective.prototype.ngOnInit = function () {
        this.el.focus();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], FocusDirective.prototype, "focus", void 0);
    FocusDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[focus]'
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], FocusDirective);
    return FocusDirective;
}());



/***/ }),

/***/ "./src/app/utility/http-interceptors/index-Interceptor.ts":
/*!****************************************************************!*\
  !*** ./src/app/utility/http-interceptors/index-Interceptor.ts ***!
  \****************************************************************/
/*! exports provided: HttpInterceptors */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpInterceptors", function() { return HttpInterceptors; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./jwt-interceptor */ "./src/app/utility/http-interceptors/jwt-interceptor.ts");


/** Http interceptor providers in outside-in order */
var HttpInterceptors = [
    { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HTTP_INTERCEPTORS"], useClass: _jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__["JWTInterceptor"], multi: true }
];


/***/ }),

/***/ "./src/app/utility/http-interceptors/jwt-interceptor.ts":
/*!**************************************************************!*\
  !*** ./src/app/utility/http-interceptors/jwt-interceptor.ts ***!
  \**************************************************************/
/*! exports provided: JWTInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JWTInterceptor", function() { return JWTInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @services/shared.service */ "./src/app/utility/shared-service/shared.service.ts");
/* harmony import */ var _constants_base_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @constants/base-constants */ "./src/app/utility/constants/base-constants.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var JWTInterceptor = /** @class */ (function () {
    function JWTInterceptor(_sharedService) {
        this._sharedService = _sharedService;
    }
    JWTInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        var jwtReq = request.clone();
        // Pass on the cloned request instead of the original request.
        return next.handle(jwtReq).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (event) {
            if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]) {
                var accessToken = event.headers.get('X-Auth');
                if (accessToken) {
                    _this._sharedService.setToken(accessToken);
                }
            }
        }, function (err) {
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpErrorResponse"]) {
                var message = 'Could not process the request. Please try again.';
                if (err.error && err.error['errorMessages'] && err.error['errorMessages'].length > 0) {
                    _this._sharedService.setToastMessage(err.error['errorMessages'][0], _constants_base_constants__WEBPACK_IMPORTED_MODULE_4__["ToastStatus"].ERROR);
                }
                else if (err.status !== _constants_base_constants__WEBPACK_IMPORTED_MODULE_4__["HttpStatus"].EXPIRED) {
                    message = err.error['message'] ? err.error['message'] : message;
                    _this._sharedService.setToastMessage(message, _constants_base_constants__WEBPACK_IMPORTED_MODULE_4__["ToastStatus"].ERROR);
                }
                if (err.status === _constants_base_constants__WEBPACK_IMPORTED_MODULE_4__["HttpStatus"].UNAUTHORIZED) {
                    _this._sharedService.logout();
                }
            }
        }));
    };
    JWTInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_services_shared_service__WEBPACK_IMPORTED_MODULE_3__["SharedService"]])
    ], JWTInterceptor);
    return JWTInterceptor;
}());



/***/ }),

/***/ "./src/app/utility/pipes/checkEmpty.pipe.ts":
/*!**************************************************!*\
  !*** ./src/app/utility/pipes/checkEmpty.pipe.ts ***!
  \**************************************************/
/*! exports provided: CheckEmptyPipe, PlaceNAPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckEmptyPipe", function() { return CheckEmptyPipe; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlaceNAPipe", function() { return PlaceNAPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var CheckEmptyPipe = /** @class */ (function () {
    function CheckEmptyPipe() {
    }
    CheckEmptyPipe.prototype.transform = function (value) {
        if (value === '' || value === null) {
            return '-';
        }
        else {
            return value;
        }
    };
    CheckEmptyPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'checkEmpty' })
    ], CheckEmptyPipe);
    return CheckEmptyPipe;
}());

var PlaceNAPipe = /** @class */ (function () {
    function PlaceNAPipe() {
    }
    PlaceNAPipe.prototype.transform = function (value) {
        if (value === '' || value === null || value === undefined) {
            return 'N/A';
        }
        else {
            return value;
        }
    };
    PlaceNAPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'placeNA' })
    ], PlaceNAPipe);
    return PlaceNAPipe;
}());



/***/ }),

/***/ "./src/app/utility/pipes/filter-pipe.pipe.ts":
/*!***************************************************!*\
  !*** ./src/app/utility/pipes/filter-pipe.pipe.ts ***!
  \***************************************************/
/*! exports provided: SortPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SortPipe", function() { return SortPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SortPipe = /** @class */ (function () {
    function SortPipe() {
    }
    SortPipe.prototype.transform = function (array, args, order) {
        if (order === void 0) { order = 'asc'; }
        if (order === 'asc') {
            array.sort(function (a, b) {
                if (a[args] === b[args]) {
                    return 0;
                }
                return a[args] > b[args] ? 1 : -1;
            });
        }
        else if (order === 'desc') {
            array.sort(function (a, b) {
                if (a[args] === b[args]) {
                    return 0;
                }
                return a[args] > b[args] ? -1 : 1;
            });
        }
        return array;
    };
    SortPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'sortBy'
        })
    ], SortPipe);
    return SortPipe;
}());



/***/ }),

/***/ "./src/app/utility/pipes/locale-number.pipe.ts":
/*!*****************************************************!*\
  !*** ./src/app/utility/pipes/locale-number.pipe.ts ***!
  \*****************************************************/
/*! exports provided: LocaleNumberPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocaleNumberPipe", function() { return LocaleNumberPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var LocaleNumberPipe = /** @class */ (function () {
    function LocaleNumberPipe() {
    }
    LocaleNumberPipe.prototype.transform = function (value, maximumFractionDigits, locale) {
        if (locale === void 0) { locale = 'en-IN'; }
        return new Intl.NumberFormat(locale, { maximumFractionDigits: maximumFractionDigits }).format(value);
    };
    LocaleNumberPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'localeNumber'
        })
    ], LocaleNumberPipe);
    return LocaleNumberPipe;
}());



/***/ }),

/***/ "./src/app/utility/pipes/timer.pipe.ts":
/*!*********************************************!*\
  !*** ./src/app/utility/pipes/timer.pipe.ts ***!
  \*********************************************/
/*! exports provided: FormatTimePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormatTimePipe", function() { return FormatTimePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FormatTimePipe = /** @class */ (function () {
    function FormatTimePipe() {
    }
    FormatTimePipe.prototype.transform = function (value) {
        var hours = Math.floor(value / 3600);
        var minutes = Math.floor((value % 3600) / 60);
        return ('00' + hours).slice(-2) + ':' + ('00' + minutes).slice(-2) + ':' + ('00' + Math.floor(value - minutes * 60)).slice(-2);
    };
    FormatTimePipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'formatTime'
        })
    ], FormatTimePipe);
    return FormatTimePipe;
}());



/***/ }),

/***/ "./src/app/utility/shared-component/page-not-found/page-not-found.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/utility/shared-component/page-not-found/page-not-found.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".body-content {\n  min-height: 550px !important;\n}\n\n.page-not-found-section {\n\n}\n\n.notFoundImage {\n  margin-top: 130px;\n  margin-left: 100px;\n}\n\n.notFoundContent {\n  float: right;\n  margin-top: 50px;\n  width: 600px;\n  height: 500px;\n  font-size: 35px;\n  background: #d84315;\n  text-align: center;\n  color: #fff;\n  border-radius: 100% 0px 0px 0;\n}\n\n.notFoundContent p {\n  line-height: 5em;\n  margin-top: 253px;\n  margin-left: 113px;\n}\n\n.backToHome {\n  margin-top: 89px;\n  margin-left: 50px;\n  font-size: 26px;\n}\n\n.backToHome a {\n  color: #d84315;\n  font-weight: bold;\n  text-decoration: underline;\n}\n"

/***/ }),

/***/ "./src/app/utility/shared-component/page-not-found/page-not-found.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/utility/shared-component/page-not-found/page-not-found.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-not-found-section\">\n  <img src=\"404.png\" class=\"notFoundImage\">\n  <div class=\"notFoundContent\">\n    <p>Page not found!</p>\n  </div>\n  <div class=\"backToHome\">Back to <a (click)=\"returnHome()\">home</a></div>\n</div>\n"

/***/ }),

/***/ "./src/app/utility/shared-component/page-not-found/page-not-found.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/utility/shared-component/page-not-found/page-not-found.component.ts ***!
  \*************************************************************************************/
/*! exports provided: PageNotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageNotFoundComponent", function() { return PageNotFoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants_routes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../constants/routes */ "./src/app/utility/constants/routes.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent(router) {
        this.router = router;
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponent.prototype.returnHome = function () {
        this.router.navigate(['/' + _constants_routes__WEBPACK_IMPORTED_MODULE_1__["RouteConstants"].LOGIN]);
    };
    PageNotFoundComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-page-not-found',
            template: __webpack_require__(/*! ./page-not-found.component.html */ "./src/app/utility/shared-component/page-not-found/page-not-found.component.html"),
            styles: [__webpack_require__(/*! ./page-not-found.component.css */ "./src/app/utility/shared-component/page-not-found/page-not-found.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());



/***/ }),

/***/ "./src/app/utility/shared-component/progress-hud/progress-hud.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/utility/shared-component/progress-hud/progress-hud.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"isLoading\" class=\"inner-loader\">\n  <div class=\"loader\">\n    <img src=\"assets/images/loading.gif\">\n  </div>\n</div>\n\n<div *ngIf=\"isLoading\" class=\"white-fixed-layer\"></div>\n"

/***/ }),

/***/ "./src/app/utility/shared-component/progress-hud/progress-hud.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/utility/shared-component/progress-hud/progress-hud.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".inner-loader {\n  display: inline;\n  position: fixed;\n  top: 0;\n  z-index: 9999999 !important;\n  width: 100%;\n  text-align: center;\n  height: 100%;\n  background-color: rgba(243, 243, 243, 0.6);\n  left: 0;\n  padding-top: 18%; }\n\n.inner-loader span {\n  font-size: 16px;\n  font-weight: bold;\n  color: #000; }\n\n.inner-loader span img {\n  width: 60px; }\n\n.white-fixed-layer {\n  position: fixed;\n  height: 100%;\n  width: 100%;\n  background: rgba(255, 255, 255, 0);\n  z-index: 9999 !important; }\n"

/***/ }),

/***/ "./src/app/utility/shared-component/progress-hud/progress-hud.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/utility/shared-component/progress-hud/progress-hud.component.ts ***!
  \*********************************************************************************/
/*! exports provided: ProgressHudComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgressHudComponent", function() { return ProgressHudComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_service_shared_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared-service/shared.service */ "./src/app/utility/shared-service/shared.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProgressHudComponent = /** @class */ (function () {
    function ProgressHudComponent(sharedService) {
        this.sharedService = sharedService;
        this.isLoading = false;
    }
    ProgressHudComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sharedService.getLoader().subscribe(function (isLoading) {
            _this.isLoading = isLoading;
        });
    };
    ProgressHudComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-progress-hud',
            template: __webpack_require__(/*! ./progress-hud.component.html */ "./src/app/utility/shared-component/progress-hud/progress-hud.component.html"),
            styles: [__webpack_require__(/*! ./progress-hud.component.scss */ "./src/app/utility/shared-component/progress-hud/progress-hud.component.scss")]
        }),
        __metadata("design:paramtypes", [_shared_service_shared_service__WEBPACK_IMPORTED_MODULE_1__["SharedService"]])
    ], ProgressHudComponent);
    return ProgressHudComponent;
}());



/***/ }),

/***/ "./src/app/utility/shared-component/toast/toast.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/utility/shared-component/toast/toast.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class='toast-multiple'>\n  <li *ngFor=\"let message of messages\">\n    {{message}}\n  </li>\n</ul>\n"

/***/ }),

/***/ "./src/app/utility/shared-component/toast/toast.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/utility/shared-component/toast/toast.component.ts ***!
  \*******************************************************************/
/*! exports provided: ToastComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastComponent", function() { return ToastComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ToastComponent = /** @class */ (function () {
    function ToastComponent() {
    }
    ToastComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ToastComponent.prototype, "messages", void 0);
    ToastComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-toast',
            template: __webpack_require__(/*! ./toast.component.html */ "./src/app/utility/shared-component/toast/toast.component.html"),
        }),
        __metadata("design:paramtypes", [])
    ], ToastComponent);
    return ToastComponent;
}());



/***/ }),

/***/ "./src/app/utility/shared-component/validation/validation.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/utility/shared-component/validation/validation.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".errorMsgStyle span{\n  color: red;\n  top: 0px;\n  position: relative;\n}\n"

/***/ }),

/***/ "./src/app/utility/shared-component/validation/validation.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/utility/shared-component/validation/validation.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<small class=\"errorMsgStyle\">\n  <span>{{errMsg}}</span>\n</small>\n"

/***/ }),

/***/ "./src/app/utility/shared-component/validation/validation.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/utility/shared-component/validation/validation.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ValidationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidationComponent", function() { return ValidationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ValidationComponent = /** @class */ (function () {
    function ValidationComponent() {
    }
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], ValidationComponent.prototype, "errMsg", void 0);
    ValidationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'validation',
            template: __webpack_require__(/*! ./validation.component.html */ "./src/app/utility/shared-component/validation/validation.component.html"),
            styles: [__webpack_require__(/*! ./validation.component.css */ "./src/app/utility/shared-component/validation/validation.component.css")]
        })
    ], ValidationComponent);
    return ValidationComponent;
}());



/***/ }),

/***/ "./src/app/utility/shared-functions/common-functions.ts":
/*!**************************************************************!*\
  !*** ./src/app/utility/shared-functions/common-functions.ts ***!
  \**************************************************************/
/*! exports provided: isEmpty, AppLogger, ClipboardText, SetupScrollClass */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isEmpty", function() { return isEmpty; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppLogger", function() { return AppLogger; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClipboardText", function() { return ClipboardText; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SetupScrollClass", function() { return SetupScrollClass; });
/* harmony import */ var _constants_base_constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../constants/base-constants */ "./src/app/utility/constants/base-constants.ts");

var isEmpty = function (obj) { return Object.keys(obj).length === 0 && obj.constructor === Object; };
var AppLogger = function (value) {
    // console.log(`<------------------------------------ ${value} ------------------------------------>`);
};
function ClipboardText(value) {
    try {
        var inp = document.createElement('input');
        document.body.appendChild(inp);
        inp.value = value;
        inp.select();
        document.execCommand('copy', false);
        inp.remove();
        return true;
    }
    catch (e) {
        return false;
    }
}
function SetupScrollClass(popupState) {
    popupState === _constants_base_constants__WEBPACK_IMPORTED_MODULE_0__["PopupState"].Open ? $("body").addClass("scroll-hide") : $("body").removeClass("scroll-hide");
}


/***/ }),

/***/ "./src/app/utility/shared-functions/encryption-functions.ts":
/*!******************************************************************!*\
  !*** ./src/app/utility/shared-functions/encryption-functions.ts ***!
  \******************************************************************/
/*! exports provided: EncryptionFunctions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EncryptionFunctions", function() { return EncryptionFunctions; });
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _constants_base_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @constants/base-constants */ "./src/app/utility/constants/base-constants.ts");


var EncryptionFunctions = /** @class */ (function () {
    function EncryptionFunctions() {
    }
    EncryptionFunctions.ENCRYPT_OBJ = function (value) {
        return crypto_js__WEBPACK_IMPORTED_MODULE_0__["AES"].encrypt(JSON.stringify(value), _constants_base_constants__WEBPACK_IMPORTED_MODULE_1__["BASE"].ENCRYPTION_TOKEN);
    };
    EncryptionFunctions.DECRYPT_OBJ = function (value) {
        if (value && value != null) {
            var bytes = crypto_js__WEBPACK_IMPORTED_MODULE_0__["AES"].decrypt(value.toString(), _constants_base_constants__WEBPACK_IMPORTED_MODULE_1__["BASE"].ENCRYPTION_TOKEN);
            var decryptedData = JSON.parse(bytes.toString(crypto_js__WEBPACK_IMPORTED_MODULE_0__["enc"].Utf8));
            return decryptedData;
        }
        return '';
    };
    return EncryptionFunctions;
}());



/***/ }),

/***/ "./src/app/utility/shared-module/material/material.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/utility/shared-module/material/material.module.ts ***!
  \*******************************************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"]
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"]
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/utility/shared-service/apimanager.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/utility/shared-service/apimanager.service.ts ***!
  \**************************************************************/
/*! exports provided: APIManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APIManager", function() { return APIManager; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shared_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared.service */ "./src/app/utility/shared-service/shared.service.ts");
/* harmony import */ var _constants_base_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../constants/base-constants */ "./src/app/utility/constants/base-constants.ts");
/* harmony import */ var _shared_functions_common_functions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared-functions/common-functions */ "./src/app/utility/shared-functions/common-functions.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var APIManager = /** @class */ (function () {
    function APIManager(sharedService, http) {
        this.sharedService = sharedService;
        this.http = http;
    }
    APIManager.prototype.getAPI = function (endPoint, params, searchParams, httpOptions, showLoader, showToast) {
        var _this = this;
        if (params === void 0) { params = {}; }
        if (searchParams === void 0) { searchParams = {}; }
        if (httpOptions === void 0) { httpOptions = this.HttpOptions; }
        if (showLoader === void 0) { showLoader = true; }
        if (showToast === void 0) { showToast = false; }
        if (showLoader) {
            this.sharedService.setLoader(true);
        }
        return this.http.get(this.prepareEndpoint(endPoint, params, searchParams), httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) {
            return _this.extractData(res, showToast);
        }, function (error) {
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            if (showLoader) {
                _this.sharedService.setLoader(false);
            }
        }));
    };
    APIManager.prototype.getImage = function (endPoint) {
        return this.http.get(endPoint, this.HttpOptions_4)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) {
            return res;
        }), function (error) {
            return error;
        });
    };
    APIManager.prototype.postAPI = function (endPoint, params, httpOptions, showLoader, showToast) {
        var _this = this;
        if (params === void 0) { params = {}; }
        if (httpOptions === void 0) { httpOptions = this.HttpOptions; }
        if (showLoader === void 0) { showLoader = true; }
        if (showToast === void 0) { showToast = true; }
        if (showLoader) {
            this.sharedService.setLoader(true);
        }
        return this.http.post(endPoint, params, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (response) {
            return _this.extractData(response, showToast);
        }, function (error) {
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            if (showLoader) {
                _this.sharedService.setLoader(false);
            }
        }));
    };
    APIManager.prototype.putAPI = function (endPoint, params, httpOptions, showLoader, showToast) {
        var _this = this;
        if (params === void 0) { params = {}; }
        if (httpOptions === void 0) { httpOptions = this.HttpOptions; }
        if (showLoader === void 0) { showLoader = true; }
        if (showToast === void 0) { showToast = true; }
        if (showLoader) {
            this.sharedService.setLoader(true);
        }
        return this.http.put(endPoint, params, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (response) {
            return _this.extractData(response, showToast);
        }, function (error) {
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            if (showLoader) {
                _this.sharedService.setLoader(false);
            }
        }));
    };
    /**
     * Performs a request with `put` http method.
     * */
    APIManager.prototype.putMutliPartAPI = function (url, params, filesObj, httpOptions, showLoader, showToast) {
        var _this = this;
        if (httpOptions === void 0) { httpOptions = this.HttpOptions; }
        if (showLoader === void 0) { showLoader = true; }
        if (showToast === void 0) { showToast = true; }
        if (showLoader) {
            this.sharedService.setLoader(true);
        }
        var formData = new FormData();
        for (var _i = 0, filesObj_1 = filesObj; _i < filesObj_1.length; _i++) {
            var obj = filesObj_1[_i];
            var imgFilesObj = obj['files'];
            for (var i = 0; i < imgFilesObj.length; i++) {
                formData.append(obj['reqKey'], imgFilesObj[i], imgFilesObj[i].name);
            }
        }
        if (params !== '' && params !== undefined && params !== null) {
            for (var property in params) {
                if (params.hasOwnProperty(property)) {
                    formData.append(property, params[property]);
                }
            }
        }
        return this.http.put(url, formData, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (response) {
            return _this.extractData(response, showToast);
        }, function (error) {
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            if (showLoader) {
                _this.sharedService.setLoader(false);
            }
        }));
    };
    APIManager.prototype.postMultipartAPI = function (endPoint, params, filesObj, httpOptions, showLoader, showToast) {
        var _this = this;
        if (params === void 0) { params = {}; }
        if (httpOptions === void 0) { httpOptions = this.HttpOptions; }
        if (showLoader === void 0) { showLoader = true; }
        if (showToast === void 0) { showToast = true; }
        if (showLoader) {
            this.sharedService.setLoader(true);
        }
        var formData = new FormData();
        for (var _i = 0, filesObj_2 = filesObj; _i < filesObj_2.length; _i++) {
            var obj = filesObj_2[_i];
            var imgFilesObj = obj['files'];
            for (var i = 0; i < imgFilesObj.length; i++) {
                formData.append(obj['reqKey'], imgFilesObj[i], imgFilesObj[i].name);
            }
        }
        if (params && (Object.keys(params).length)) {
            for (var docKey in params) {
                formData.append(docKey, params[docKey]);
            }
        }
        return this.http.post(endPoint, formData, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (response) {
            return _this.extractData(response, showToast);
        }, function (error) {
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            if (showLoader) {
                _this.sharedService.setLoader(false);
            }
        }));
    };
    APIManager.prototype.deleteAPI = function (endPoint, params, httpOptions, showLoader, showToast) {
        var _this = this;
        if (params === void 0) { params = {}; }
        if (httpOptions === void 0) { httpOptions = this.HttpOptions; }
        if (showLoader === void 0) { showLoader = true; }
        if (showToast === void 0) { showToast = true; }
        if (showLoader) {
            this.sharedService.setLoader(true);
        }
        return this.http.delete(this.prepareEndpoint(endPoint, params), httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (response) {
            return _this.extractData(response, showToast);
        }, function (error) {
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            if (showLoader) {
                _this.sharedService.setLoader(false);
            }
        }));
    };
    Object.defineProperty(APIManager.prototype, "HttpOptions", {
        get: function () {
            var httpOptions;
            var authToken = this.sharedService.getToken();
            if (this.sharedService.IsValidToken(authToken)) {
                httpOptions = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'X-Auth': "" + authToken
                });
            }
            else {
                httpOptions = this.HttpOptions_2;
            }
            return { headers: httpOptions };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(APIManager.prototype, "HttpOptions_1", {
        get: function () {
            var httpOptions;
            var authToken = this.sharedService.getToken();
            httpOptions = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': "" + authToken
            });
            return { headers: httpOptions };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(APIManager.prototype, "HttpOptions_2", {
        get: function () {
            var httpOptions;
            httpOptions = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/json'
            });
            return { headers: httpOptions };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(APIManager.prototype, "HttpOptions_3", {
        get: function () {
            var httpOptions;
            var authToken = this.sharedService.getToken();
            httpOptions = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Authorization': "" + authToken
            });
            return { headers: httpOptions };
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(APIManager.prototype, "HttpOptions_4", {
        get: function () {
            return {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                    'Authorization': "" + this.sharedService.getToken(),
                }),
                responseType: 'blob'
            };
        },
        enumerable: true,
        configurable: true
    });
    APIManager.prototype.prepareEndpoint = function (endPoint, params, searchParams) {
        if (searchParams === void 0) { searchParams = {}; }
        if (!Object(_shared_functions_common_functions__WEBPACK_IMPORTED_MODULE_5__["isEmpty"])(searchParams)) {
            params['search'] = JSON.stringify(searchParams);
        }
        if (Object.keys(params).length) {
            var queryString = params ? '?' : '';
            var count = 0;
            for (var key in params) {
                if (params.hasOwnProperty(key)) {
                    queryString += (count > 0) ? '&' + key + '=' + params[key] : key + '=' + params[key];
                    count++;
                }
            }
            return endPoint + queryString;
        }
        return endPoint;
    };
    APIManager.prototype.extractData = function (res, show) {
        var msg = res.message;
        if (show && msg) {
            this.sharedService.setToastMessage(msg, _constants_base_constants__WEBPACK_IMPORTED_MODULE_4__["ToastStatus"].SUCCESS);
        }
        return res || {};
    };
    APIManager = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_shared_service__WEBPACK_IMPORTED_MODULE_3__["SharedService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], APIManager);
    return APIManager;
}());



/***/ }),

/***/ "./src/app/utility/shared-service/shared-user.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/utility/shared-service/shared-user.service.ts ***!
  \***************************************************************/
/*! exports provided: SharedUserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedUserService", function() { return SharedUserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constants_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../constants/storage */ "./src/app/utility/constants/storage.ts");
/* harmony import */ var _shared_functions_encryption_functions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared-functions/encryption-functions */ "./src/app/utility/shared-functions/encryption-functions.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SharedUserService = /** @class */ (function () {
    function SharedUserService() {
        this.userFlag = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](false);
    }
    SharedUserService.prototype.getUser = function () {
        if (!this._user) {
            this._user = _shared_functions_encryption_functions__WEBPACK_IMPORTED_MODULE_2__["EncryptionFunctions"].DECRYPT_OBJ(localStorage.getItem(_constants_storage__WEBPACK_IMPORTED_MODULE_1__["APPStorage"].USER));
        }
        return this._user;
    };
    SharedUserService.prototype.setUser = function (value) {
        localStorage.setItem(_constants_storage__WEBPACK_IMPORTED_MODULE_1__["APPStorage"].USER, _shared_functions_encryption_functions__WEBPACK_IMPORTED_MODULE_2__["EncryptionFunctions"].ENCRYPT_OBJ(value));
        this._user = value;
    };
    /* Shared User detailChangeFlag for update status */
    SharedUserService.prototype.setUserDetailCall = function (value) {
        this.userFlag.next(value);
    };
    SharedUserService.prototype.getUserDetailCall = function () {
        return this.userFlag.asObservable();
    };
    SharedUserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], SharedUserService);
    return SharedUserService;
}());



/***/ }),

/***/ "./src/app/utility/shared-service/shared.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/utility/shared-service/shared.service.ts ***!
  \**********************************************************/
/*! exports provided: SharedService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedService", function() { return SharedService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @auth0/angular-jwt */ "./node_modules/@auth0/angular-jwt/index.js");
/* harmony import */ var _constants_routes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants/routes */ "./src/app/utility/constants/routes.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shared-user.service */ "./src/app/utility/shared-service/shared-user.service.ts");
/* harmony import */ var _constants_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../constants/storage */ "./src/app/utility/constants/storage.ts");
/* harmony import */ var _shared_functions_encryption_functions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared-functions/encryption-functions */ "./src/app/utility/shared-functions/encryption-functions.ts");
/* harmony import */ var _common_functions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../common-functions */ "./src/app/utility/common-functions.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SharedService = /** @class */ (function (_super) {
    __extends(SharedService, _super);
    function SharedService(router) {
        var _this = _super.call(this) || this;
        _this.router = router;
        _this.jwtHelperService = new _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_2__["JwtHelperService"]();
        _this.taskCount = 0;
        _this._token = '';
        _this.isLoginRequired = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](false);
        _this.isLoading = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](false);
        _this.msgBody = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](null);
        return _this;
    }
    /* Shared Loader Param */
    SharedService.prototype.getLoader = function () {
        return this.isLoading.asObservable();
    };
    SharedService.prototype.setToken = function (value) {
        localStorage.setItem(_constants_storage__WEBPACK_IMPORTED_MODULE_6__["APPStorage"].TOKEN, _shared_functions_encryption_functions__WEBPACK_IMPORTED_MODULE_7__["EncryptionFunctions"].ENCRYPT_OBJ(value));
        this._token = value;
    };
    SharedService.prototype.getToken = function () {
        this._token = _shared_functions_encryption_functions__WEBPACK_IMPORTED_MODULE_7__["EncryptionFunctions"].DECRYPT_OBJ(localStorage.getItem(_constants_storage__WEBPACK_IMPORTED_MODULE_6__["APPStorage"].TOKEN));
        return this._token;
    };
    /* Shared User Token Param */
    SharedService.prototype.isLoggedIn = function () {
        // return this.IsValidToken(this.getToken()) && this.isValidUser(this.getUser());
        return this.IsValidToken(this.getToken());
    };
    SharedService.prototype.isValidUser = function (user) {
        return (user) ? true : false;
    };
    SharedService.prototype.IsValidToken = function (token) {
        var isValid = true;
        try {
            var isTokenExpired = this.jwtHelperService.isTokenExpired(this.getToken());
            if (isTokenExpired) {
                isValid = false;
                this.clearSession();
            }
        }
        catch (e) {
            isValid = false;
        }
        return isValid;
    };
    SharedService.prototype.setLoader = function (val) {
        if (val) {
            this.taskCount += 1;
        }
        else {
            this.taskCount -= 1;
            if (this.taskCount !== 0) {
                val = true;
            }
        }
        this.isLoading.next(val);
    };
    SharedService.prototype.getToastMessage = function () {
        return this.msgBody.asObservable();
    };
    SharedService.prototype.clearSession = function () {
        this.setToken(null);
        this.setUser(null);
        this.setLoginRequired(false);
        localStorage.clear();
    };
    SharedService.prototype.logout = function () {
        this.clearSession();
        this.router.navigate(['/' + _constants_routes__WEBPACK_IMPORTED_MODULE_3__["RouteConstants"].LOGIN]);
    };
    /* setting route */
    SharedService.prototype.setToastMessage = function (message, type, title) {
        if (title === void 0) { title = ''; }
        var body = null;
        if (message) {
            body = {
                message: message,
                type: type,
                title: title
            };
        }
        this.msgBody.next(body);
    };
    /* Shared LoggedIn Param */
    SharedService.prototype.getLoginRequired = function () {
        return this.isLoginRequired.asObservable();
    };
    SharedService.prototype.setLoginRequired = function (val) {
        this.isLoginRequired.next(val);
    };
    // set career data
    SharedService.prototype.getButtonData = function (key) {
        this.buttonData = _common_functions__WEBPACK_IMPORTED_MODULE_8__["CommonFunctions"].DECRYPT_OBJ(localStorage.getItem(_constants_storage__WEBPACK_IMPORTED_MODULE_6__["APPStorage"].BUTTONDETAIL));
        return this.buttonData[key];
    };
    SharedService.prototype.setButtonData = function (key, value) {
        var setData = _common_functions__WEBPACK_IMPORTED_MODULE_8__["CommonFunctions"].DECRYPT_OBJ(localStorage.getItem(_constants_storage__WEBPACK_IMPORTED_MODULE_6__["APPStorage"].BUTTONDETAIL)) || {};
        setData[key] = value;
        localStorage.setItem(_constants_storage__WEBPACK_IMPORTED_MODULE_6__["APPStorage"].BUTTONDETAIL, _common_functions__WEBPACK_IMPORTED_MODULE_8__["CommonFunctions"].ENCRYPT_OBJ(setData));
        this.buttonData = setData;
    };
    SharedService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], SharedService);
    return SharedService;
}(_shared_user_service__WEBPACK_IMPORTED_MODULE_5__["SharedUserService"]));



/***/ }),

/***/ "./src/app/utility/utility.module.ts":
/*!*******************************************!*\
  !*** ./src/app/utility/utility.module.ts ***!
  \*******************************************/
/*! exports provided: UtilityModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UtilityModule", function() { return UtilityModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _base_component_base_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./base-component/base.component */ "./src/app/utility/base-component/base.component.ts");
/* harmony import */ var _shared_component_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shared-component/page-not-found/page-not-found.component */ "./src/app/utility/shared-component/page-not-found/page-not-found.component.ts");
/* harmony import */ var _shared_component_progress_hud_progress_hud_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shared-component/progress-hud/progress-hud.component */ "./src/app/utility/shared-component/progress-hud/progress-hud.component.ts");
/* harmony import */ var _directives_focus_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./directives/focus.directive */ "./src/app/utility/directives/focus.directive.ts");
/* harmony import */ var _directives_disable_control_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./directives/disable-control.directive */ "./src/app/utility/directives/disable-control.directive.ts");
/* harmony import */ var _pipes_checkEmpty_pipe__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pipes/checkEmpty.pipe */ "./src/app/utility/pipes/checkEmpty.pipe.ts");
/* harmony import */ var _pipes_filter_pipe_pipe__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pipes/filter-pipe.pipe */ "./src/app/utility/pipes/filter-pipe.pipe.ts");
/* harmony import */ var _pipes_locale_number_pipe__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pipes/locale-number.pipe */ "./src/app/utility/pipes/locale-number.pipe.ts");
/* harmony import */ var _pipes_timer_pipe__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pipes/timer.pipe */ "./src/app/utility/pipes/timer.pipe.ts");
/* harmony import */ var _http_interceptors_index_Interceptor__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./http-interceptors/index-Interceptor */ "./src/app/utility/http-interceptors/index-Interceptor.ts");
/* harmony import */ var _shared_component_toast_toast_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./shared-component/toast/toast.component */ "./src/app/utility/shared-component/toast/toast.component.ts");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @services/shared.service */ "./src/app/utility/shared-service/shared.service.ts");
/* harmony import */ var _services_apimanager_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @services/apimanager.service */ "./src/app/utility/shared-service/apimanager.service.ts");
/* harmony import */ var _services_shared_user_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @services/shared-user.service */ "./src/app/utility/shared-service/shared-user.service.ts");
/* harmony import */ var _shared_module_material_material_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./shared-module/material/material.module */ "./src/app/utility/shared-module/material/material.module.ts");
/* harmony import */ var _shared_component_validation_validation_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./shared-component/validation/validation.component */ "./src/app/utility/shared-component/validation/validation.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















var UtilityModule = /** @class */ (function () {
    function UtilityModule() {
    }
    UtilityModule_1 = UtilityModule;
    UtilityModule.forRoot = function () {
        return {
            ngModule: UtilityModule_1,
            providers: [_http_interceptors_index_Interceptor__WEBPACK_IMPORTED_MODULE_12__["HttpInterceptors"],
                _services_shared_service__WEBPACK_IMPORTED_MODULE_14__["SharedService"],
                _services_shared_user_service__WEBPACK_IMPORTED_MODULE_16__["SharedUserService"],
                _services_apimanager_service__WEBPACK_IMPORTED_MODULE_15__["APIManager"]]
        };
    };
    UtilityModule = UtilityModule_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _shared_module_material_material_module__WEBPACK_IMPORTED_MODULE_17__["MaterialModule"]
            ],
            declarations: [
                _base_component_base_component__WEBPACK_IMPORTED_MODULE_3__["BaseComponent"],
                _shared_component_page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_4__["PageNotFoundComponent"],
                _shared_component_progress_hud_progress_hud_component__WEBPACK_IMPORTED_MODULE_5__["ProgressHudComponent"],
                _shared_component_validation_validation_component__WEBPACK_IMPORTED_MODULE_18__["ValidationComponent"],
                _shared_component_toast_toast_component__WEBPACK_IMPORTED_MODULE_13__["ToastComponent"],
                _directives_focus_directive__WEBPACK_IMPORTED_MODULE_6__["FocusDirective"],
                _directives_disable_control_directive__WEBPACK_IMPORTED_MODULE_7__["DisableControlDirective"],
                _pipes_checkEmpty_pipe__WEBPACK_IMPORTED_MODULE_8__["CheckEmptyPipe"],
                _pipes_checkEmpty_pipe__WEBPACK_IMPORTED_MODULE_8__["PlaceNAPipe"],
                _pipes_filter_pipe_pipe__WEBPACK_IMPORTED_MODULE_9__["SortPipe"],
                _pipes_locale_number_pipe__WEBPACK_IMPORTED_MODULE_10__["LocaleNumberPipe"],
                _pipes_timer_pipe__WEBPACK_IMPORTED_MODULE_11__["FormatTimePipe"],
            ],
            providers: [],
            exports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _shared_module_material_material_module__WEBPACK_IMPORTED_MODULE_17__["MaterialModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _directives_focus_directive__WEBPACK_IMPORTED_MODULE_6__["FocusDirective"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _base_component_base_component__WEBPACK_IMPORTED_MODULE_3__["BaseComponent"],
                _shared_component_validation_validation_component__WEBPACK_IMPORTED_MODULE_18__["ValidationComponent"],
                _directives_disable_control_directive__WEBPACK_IMPORTED_MODULE_7__["DisableControlDirective"]
            ]
        })
    ], UtilityModule);
    return UtilityModule;
    var UtilityModule_1;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    apiUrl: '',
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/smart/krishna/projects/iot-tracker/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map